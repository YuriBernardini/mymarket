﻿namespace MyMarket
{
    partial class CassiereVendita
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CassiereVendita));
            this.nomeLabel = new System.Windows.Forms.Label();
            this.EANLabel = new System.Windows.Forms.Label();
            this.EANtextBox = new System.Windows.Forms.TextBox();
            this.indietroBox = new System.Windows.Forms.PictureBox();
            this.vendiButton = new System.Windows.Forms.Button();
            this.dataLabel = new System.Windows.Forms.Label();
            this.inserisciBtn = new System.Windows.Forms.Button();
            this.numeroScontrino = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idPagamento = new System.Windows.Forms.ComboBox();
            this.quantita = new System.Windows.Forms.NumericUpDown();
            this.pagamento = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.indietroBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantita)).BeginInit();
            this.SuspendLayout();
            // 
            // nomeLabel
            // 
            this.nomeLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.nomeLabel.AutoSize = true;
            this.nomeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nomeLabel.ForeColor = System.Drawing.Color.Black;
            this.nomeLabel.Location = new System.Drawing.Point(236, 96);
            this.nomeLabel.Name = "nomeLabel";
            this.nomeLabel.Size = new System.Drawing.Size(662, 42);
            this.nomeLabel.TabIndex = 0;
            this.nomeLabel.Text = "CASSIERE: Nome Cognome 123456";
            // 
            // EANLabel
            // 
            this.EANLabel.AutoSize = true;
            this.EANLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EANLabel.Location = new System.Drawing.Point(166, 184);
            this.EANLabel.Name = "EANLabel";
            this.EANLabel.Size = new System.Drawing.Size(232, 31);
            this.EANLabel.TabIndex = 1;
            this.EANLabel.Text = "EAN PRODOTTO";
            // 
            // EANtextBox
            // 
            this.EANtextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EANtextBox.Location = new System.Drawing.Point(420, 177);
            this.EANtextBox.Name = "EANtextBox";
            this.EANtextBox.Size = new System.Drawing.Size(263, 38);
            this.EANtextBox.TabIndex = 2;
            this.EANtextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SoloNumeri);
            // 
            // indietroBox
            // 
            this.indietroBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.indietroBox.Image = ((System.Drawing.Image)(resources.GetObject("indietroBox.Image")));
            this.indietroBox.Location = new System.Drawing.Point(12, 12);
            this.indietroBox.Name = "indietroBox";
            this.indietroBox.Size = new System.Drawing.Size(168, 126);
            this.indietroBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.indietroBox.TabIndex = 24;
            this.indietroBox.TabStop = false;
            this.indietroBox.Click += new System.EventHandler(this.Indietro);
            // 
            // vendiButton
            // 
            this.vendiButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.vendiButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.vendiButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.vendiButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vendiButton.Location = new System.Drawing.Point(810, 607);
            this.vendiButton.Name = "vendiButton";
            this.vendiButton.Size = new System.Drawing.Size(528, 57);
            this.vendiButton.TabIndex = 31;
            this.vendiButton.Text = "EMETTI SCONTRINO";
            this.vendiButton.UseVisualStyleBackColor = false;
            this.vendiButton.Click += new System.EventHandler(this.Vendi);
            // 
            // dataLabel
            // 
            this.dataLabel.AutoSize = true;
            this.dataLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataLabel.Location = new System.Drawing.Point(186, 12);
            this.dataLabel.Name = "dataLabel";
            this.dataLabel.Size = new System.Drawing.Size(120, 25);
            this.dataLabel.TabIndex = 32;
            this.dataLabel.Text = "01/02/1998";
            // 
            // inserisciBtn
            // 
            this.inserisciBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.inserisciBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.inserisciBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.inserisciBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inserisciBtn.Location = new System.Drawing.Point(793, 175);
            this.inserisciBtn.Name = "inserisciBtn";
            this.inserisciBtn.Size = new System.Drawing.Size(185, 38);
            this.inserisciBtn.TabIndex = 34;
            this.inserisciBtn.Text = "INSERISCI";
            this.inserisciBtn.UseVisualStyleBackColor = false;
            this.inserisciBtn.Click += new System.EventHandler(this.InserisciProdotto);
            // 
            // numeroScontrino
            // 
            this.numeroScontrino.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numeroScontrino.Location = new System.Drawing.Point(13, 607);
            this.numeroScontrino.Name = "numeroScontrino";
            this.numeroScontrino.Size = new System.Drawing.Size(333, 38);
            this.numeroScontrino.TabIndex = 35;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(13, 289);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1325, 312);
            this.dataGridView1.TabIndex = 36;
            // 
            // idPagamento
            // 
            this.idPagamento.Cursor = System.Windows.Forms.Cursors.Hand;
            this.idPagamento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.idPagamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idPagamento.FormattingEnabled = true;
            this.idPagamento.Location = new System.Drawing.Point(352, 606);
            this.idPagamento.Name = "idPagamento";
            this.idPagamento.Size = new System.Drawing.Size(86, 39);
            this.idPagamento.TabIndex = 37;
            this.idPagamento.SelectedIndexChanged += new System.EventHandler(this.CaricaPagamenti);
            // 
            // quantita
            // 
            this.quantita.Cursor = System.Windows.Forms.Cursors.Hand;
            this.quantita.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quantita.Location = new System.Drawing.Point(705, 175);
            this.quantita.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.quantita.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.quantita.Name = "quantita";
            this.quantita.ReadOnly = true;
            this.quantita.Size = new System.Drawing.Size(66, 38);
            this.quantita.TabIndex = 38;
            this.quantita.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // pagamento
            // 
            this.pagamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pagamento.Location = new System.Drawing.Point(444, 607);
            this.pagamento.Name = "pagamento";
            this.pagamento.Size = new System.Drawing.Size(360, 38);
            this.pagamento.TabIndex = 39;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 648);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 16);
            this.label1.TabIndex = 40;
            this.label1.Text = "Numero Scotrino";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(360, 648);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 16);
            this.label2.TabIndex = 41;
            this.label2.Text = "Pagamento";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(702, 156);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 16);
            this.label3.TabIndex = 42;
            this.label3.Text = "Quantita\'";
            // 
            // CassiereVendita
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 729);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pagamento);
            this.Controls.Add(this.quantita);
            this.Controls.Add(this.idPagamento);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.numeroScontrino);
            this.Controls.Add(this.inserisciBtn);
            this.Controls.Add(this.dataLabel);
            this.Controls.Add(this.vendiButton);
            this.Controls.Add(this.indietroBox);
            this.Controls.Add(this.EANtextBox);
            this.Controls.Add(this.EANLabel);
            this.Controls.Add(this.nomeLabel);
            this.MaximizeBox = false;
            this.Name = "CassiereVendita";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CassiereHome";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.indietroBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantita)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label nomeLabel;
        private System.Windows.Forms.Label EANLabel;
        private System.Windows.Forms.TextBox EANtextBox;
        private System.Windows.Forms.PictureBox indietroBox;
        private System.Windows.Forms.Button vendiButton;
        private System.Windows.Forms.Label dataLabel;
        private System.Windows.Forms.Button inserisciBtn;
        private System.Windows.Forms.TextBox numeroScontrino;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox idPagamento;
        private System.Windows.Forms.NumericUpDown quantita;
        private System.Windows.Forms.TextBox pagamento;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}