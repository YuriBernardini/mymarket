﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyMarket
{
    public partial class Report : Form
    {
        MyMarketDBDataContext db = new MyMarketDBDataContext();

        public Report()
        {
            InitializeComponent();

        }

        private void Indietro(object sender, EventArgs e)
        {
            ResponsabileHome resp = new ResponsabileHome();
            resp.Show();
            this.Close();
        }

        private void provenienza_Click(object sender, EventArgs e)
        {
            var res = from pp in db.PRODOTTI
                      join p in db.PRODUTTORI on pp.PartitaIVA equals p.PartitaIVA
                      join c in db.CITTA on p.IdCitta equals c.CAP
                      join ps in db.PAESI on c.IdPaese equals ps.Codice
                      select new
                      {
                          EAN = pp.EAN,
                          Prodotto = pp.Descrizione,
                          Citta = c.Nome,
                          Paese = ps.Descrizione
                      };
            dataGridView1.DataSource = res;
        }

        private void top_Click(object sender, EventArgs e)
        {
            var res = (from d in db.DETTAGLI_SCONTRINO
                      join p in db.PRODOTTI on d.EAN equals p.EAN
                      group d by new
                      {
                          d.EAN,
                          p.Descrizione
                      } into g
                      orderby g.Sum(x => x.Quantita) descending
                      select new
                      {
                          EAN = g.Key.EAN,
                          Descrizione = g.Key.Descrizione,
                          Quantita = g.Sum(x => x.Quantita)
                      }).Take(5);

            this.dataGridView1.DataSource = res;
        }

        private void incassi_Click(object sender, EventArgs e)
        {
            var res = (from d in db.DETTAGLI_SCONTRINO
                       join p in db.PRODOTTI on d.EAN equals p.EAN
                       group d by new
                       {
                           d.EAN,
                           p.Descrizione,
                           p.PrezzoVendita
                       } into g
                       orderby g.Sum(x => x.Quantita) descending
                       select new
                       {
                           EAN = g.Key.EAN,
                           Descrizione = g.Key.Descrizione,
                           TotaleIncassato = (float)g.Sum(x => x.Quantita) * g.Key.PrezzoVendita 
                       });

            this.dataGridView1.DataSource = res;
            
        }
    }
}
