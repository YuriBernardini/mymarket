﻿namespace MyMarket
{
    partial class ResponsabileHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ResponsabileHome));
            this.indietroBox = new System.Windows.Forms.PictureBox();
            this.reportBtn = new System.Windows.Forms.Button();
            this.infoLabel = new System.Windows.Forms.Label();
            this.modificaDipendentiBtn = new System.Windows.Forms.Button();
            this.dataLabel = new System.Windows.Forms.Label();
            this.visualizzaDipendentiBtn = new System.Windows.Forms.Button();
            this.meetingBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.indietroBox)).BeginInit();
            this.SuspendLayout();
            // 
            // indietroBox
            // 
            this.indietroBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.indietroBox.Image = ((System.Drawing.Image)(resources.GetObject("indietroBox.Image")));
            this.indietroBox.Location = new System.Drawing.Point(12, 12);
            this.indietroBox.Name = "indietroBox";
            this.indietroBox.Size = new System.Drawing.Size(168, 126);
            this.indietroBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.indietroBox.TabIndex = 0;
            this.indietroBox.TabStop = false;
            this.indietroBox.Click += new System.EventHandler(this.Indietro);
            // 
            // reportBtn
            // 
            this.reportBtn.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.reportBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.reportBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.reportBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reportBtn.Location = new System.Drawing.Point(44, 325);
            this.reportBtn.Name = "reportBtn";
            this.reportBtn.Size = new System.Drawing.Size(341, 38);
            this.reportBtn.TabIndex = 2;
            this.reportBtn.Text = "Report";
            this.reportBtn.UseVisualStyleBackColor = false;
            this.reportBtn.Click += new System.EventHandler(this.Report);
            // 
            // infoLabel
            // 
            this.infoLabel.AutoSize = true;
            this.infoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.infoLabel.Location = new System.Drawing.Point(335, 50);
            this.infoLabel.Name = "infoLabel";
            this.infoLabel.Size = new System.Drawing.Size(557, 42);
            this.infoLabel.TabIndex = 3;
            this.infoLabel.Text = "Responsabile : nomeCongome";
            // 
            // modificaDipendentiBtn
            // 
            this.modificaDipendentiBtn.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.modificaDipendentiBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.modificaDipendentiBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.modificaDipendentiBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modificaDipendentiBtn.Location = new System.Drawing.Point(44, 244);
            this.modificaDipendentiBtn.Name = "modificaDipendentiBtn";
            this.modificaDipendentiBtn.Size = new System.Drawing.Size(341, 38);
            this.modificaDipendentiBtn.TabIndex = 4;
            this.modificaDipendentiBtn.Text = "Modifica Dipendenti";
            this.modificaDipendentiBtn.UseVisualStyleBackColor = false;
            this.modificaDipendentiBtn.Click += new System.EventHandler(this.ModificaDipendenti);
            // 
            // dataLabel
            // 
            this.dataLabel.AutoSize = true;
            this.dataLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataLabel.Location = new System.Drawing.Point(186, 12);
            this.dataLabel.Name = "dataLabel";
            this.dataLabel.Size = new System.Drawing.Size(120, 25);
            this.dataLabel.TabIndex = 33;
            this.dataLabel.Text = "01/02/1998";
            // 
            // visualizzaDipendentiBtn
            // 
            this.visualizzaDipendentiBtn.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.visualizzaDipendentiBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.visualizzaDipendentiBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.visualizzaDipendentiBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.visualizzaDipendentiBtn.Location = new System.Drawing.Point(477, 244);
            this.visualizzaDipendentiBtn.Name = "visualizzaDipendentiBtn";
            this.visualizzaDipendentiBtn.Size = new System.Drawing.Size(398, 38);
            this.visualizzaDipendentiBtn.TabIndex = 34;
            this.visualizzaDipendentiBtn.Text = "Visualizza Tutti Dipendenti";
            this.visualizzaDipendentiBtn.UseVisualStyleBackColor = false;
            this.visualizzaDipendentiBtn.Click += new System.EventHandler(this.visualizzaDipendentiBtn_Click);
            // 
            // meetingBtn
            // 
            this.meetingBtn.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.meetingBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.meetingBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.meetingBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.meetingBtn.Location = new System.Drawing.Point(477, 325);
            this.meetingBtn.Name = "meetingBtn";
            this.meetingBtn.Size = new System.Drawing.Size(398, 38);
            this.meetingBtn.TabIndex = 35;
            this.meetingBtn.Text = "Gestione Meeting";
            this.meetingBtn.UseVisualStyleBackColor = false;
            this.meetingBtn.Click += new System.EventHandler(this.meetingBtn_Click);
            // 
            // ResponsabileHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 729);
            this.Controls.Add(this.meetingBtn);
            this.Controls.Add(this.visualizzaDipendentiBtn);
            this.Controls.Add(this.dataLabel);
            this.Controls.Add(this.modificaDipendentiBtn);
            this.Controls.Add(this.infoLabel);
            this.Controls.Add(this.reportBtn);
            this.Controls.Add(this.indietroBox);
            this.MaximizeBox = false;
            this.Name = "ResponsabileHome";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form2";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.indietroBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox indietroBox;
        private System.Windows.Forms.Button reportBtn;
        private System.Windows.Forms.Label infoLabel;
        private System.Windows.Forms.Button modificaDipendentiBtn;
        private System.Windows.Forms.Label dataLabel;
        private System.Windows.Forms.Button visualizzaDipendentiBtn;
        private System.Windows.Forms.Button meetingBtn;
    }
}