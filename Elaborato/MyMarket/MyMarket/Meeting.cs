﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyMarket
{
    public partial class Meeting : Form
    {
        public Meeting()
        {
            InitializeComponent();
            stampaMeeting();
            aggiungiResponsabili();
            aggiungiRappresentanti();
        }

        MyMarketDBDataContext db = new MyMarketDBDataContext();

        private void aggiungiResponsabili()
        {
            var res = from r in db.RESPONSABILI
                      orderby r.Badge
                      join p in db.PERSONALE on r.Badge equals p.Badge
                      select new { p.Badge, p.Nome, p.Cognome };

            idResponsabile.Items.Clear();
            foreach (var x in res)
            {
                idResponsabile.Items.Add(x.Badge);
            }

        }
        private void aggiungiRappresentanti()
        {
            var res = from r in db.RAPPRESENTANTI
                      orderby r.CodiceIdentificativo
                      join p in db.PRODUTTORI on r.IdAzienda equals p.PartitaIVA
                      select new { r.CodiceIdentificativo, r.Nome };
            
            idRappresentante.Items.Clear();
            foreach (var x in res)
            {
                idRappresentante.Items.Add(x.CodiceIdentificativo);
            }

        }

        private void aggiorna_Click(object sender, EventArgs e)
        {
            aggiungiMeeting();

            stampaMeeting();

            aggiungiRappresentanti();
            aggiungiResponsabili();
        }

        private void aggiungiMeeting()
        {
            if(CampiOk())
            {
                errore.Visible = false;

                MEETING m = new MEETING();
                m.Data = AppData.Instance.DataSelezionata;
                m.Conclusione = conclusione.Text;
                m.Motivo = motivo.Text;
                m.Titolo = titolo.Text;
                m.PropostaCliente = propostaCliente.Text;
                m.PropostaRappresentante = propostaRappresentante.Text;
                m.Numero = AppData.Instance.MeetingDaInserire();
                m.IdRappresentante = Decimal.Parse(idRappresentante.SelectedItem.ToString());
                m.IdResponsabile = Decimal.Parse(idResponsabile.SelectedItem.ToString());

                db.MEETING.InsertOnSubmit(m);
                db.SubmitChanges();

            }else
            {
                errore.Visible = true;
            }
        }

        private bool CampiOk()
        {
            if (titolo.Text == "")
                return false;
            if (motivo.Text == "")
                return false;
            if (propostaCliente.Text == "")
                return false;
            if (propostaRappresentante.Text == "")
                return false;
            if (conclusione.Text == "")
                return false;
            if (idRappresentante.Text == "")
                return false;
            if (idResponsabile.Text == "")
                return false;
            return true;
        }

        private void stampaMeeting()
        {
            var res = from pp in db.PRODUTTORI
                      join r in db.RAPPRESENTANTI on pp.PartitaIVA equals r.IdAzienda
                      join m in db.MEETING on r.CodiceIdentificativo equals m.IdRappresentante
                      join rs in db.RESPONSABILI on m.IdResponsabile equals rs.Badge
                      join p in db.PERSONALE on rs.Badge equals p.Badge
                      select new
                      {
                          Numero = m.Numero,
                          Titolo = m.Titolo,
                          Data = m.Data,
                          Motivo = m.Motivo,
                          PropostaRappresentante = m.PropostaRappresentante,
                          PropostaResponsabile = m.PropostaCliente,
                          Conclusione = m.Conclusione,
                          IdRappresentante = r.CodiceIdentificativo,
                          NomeRappresentante = r.Nome,
                          CognomeRappresentante = r.Cognome,
                          Azienda = pp.Intestazione,
                          IdResponsabile = rs.Badge,
                          NomeResponsabile = p.Nome,
                          CognomeResponsabile = p.Cognome

                      };

            dataGridView1.DataSource = res;
        }

        private void aggiornaResponsabile(object sender, EventArgs e)
        {
            var res = from r in db.RESPONSABILI
                      where r.Badge == Decimal.Parse(idResponsabile.SelectedItem.ToString())
                      join p in db.PERSONALE on r.Badge equals p.Badge
                      select new { p.Badge, p.Nome, p.Cognome };

            responsabile.Text = "";
            foreach (var x in res)
            {
                responsabile.Text = x.Cognome + " " + x.Nome;
            }
        }

        private void aggiornaRappresentante(object sender, EventArgs e)
        {
            var res = from r in db.RAPPRESENTANTI
                      where r.CodiceIdentificativo == Decimal.Parse(idRappresentante.SelectedItem.ToString())
                      join p in db.PRODUTTORI on r.IdAzienda equals p.PartitaIVA
                      select new { p.Intestazione, r.Nome, r.Cognome, r.CodiceIdentificativo };

            rappresentante.Text = "";
            foreach (var x in res)
            {
                rappresentante.Text = x.Nome + " " + x.Cognome + " " + x.Intestazione;
            }
        }

        private void indietroBox_Click(object sender, EventArgs e)
        {
            ResponsabileHome responsabile = new ResponsabileHome();
            responsabile.Show();
            this.Close();
        }
    }
}
