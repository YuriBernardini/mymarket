﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyMarket
{
    public partial class GestioneCorsie : Form
    {
        MyMarketDBDataContext db = new MyMarketDBDataContext();
        public GestioneCorsie()
        {
            InitializeComponent();
            caricaResponsabilita();
            caricaLuogoProdotti();
            caricaScaffali();
        }

        private void caricaResponsabilita()
        {
            var res = from c in db.CORSIE
                      join r in db.RESPONSABILITA on c.Numero equals r.NumeroCorsia
                      join s in db.SCAFFALISTI on r.Badge equals s.Badge
                      join p in db.PERSONALE on s.Badge equals p.Badge
                      select new
                      {
                          NumeroCorsia = c.Numero,
                          BadgeScaffalista = p.Badge,
                          NomeScaffalista = p.Nome,
                          CognomeScaffalista = p.Cognome
                      };
            respGridView1.DataSource = res;
        }

        private void caricaScaffali()
        {
            var res = from s in db.SCAFFALI
                      orderby s.Matricola ascending
                      select s.Matricola;
            foreach(var x in res)
            {
                scafComboBox.Items.Add(x);
            }
        }

        private void caricaLuogoProdotti()
        {
            var res = from c in db.CORSIE
                      join s in db.SCAFFALI on c.Numero equals s.NumeroCorsia
                      join pos in db.POSIZIONI on s.Matricola equals pos.MatricolaScaffale
                      join p in db.PRODOTTI on pos.EAN equals p.EAN
                      orderby p.EAN
                      select new
                      {
                          Corsia = c.Numero,
                          MatricolaScaffale = s.Matricola,
                          EAN = p.EAN,
                          Prodotto = p.Descrizione
                      };
            this.prodGridView2.DataSource = res;

        }


        private void Indietro(object sender, EventArgs e)
        {
            ScaffalistaHome ind = new ScaffalistaHome();
            ind.Show();
            this.Close();
        }

        private void SoloNumeri(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) )
            {
                e.Handled = true;
            }
        }

        private void caricaCorsia(object sender, EventArgs e)
        {
            var res = from s in db.SCAFFALI
                      where s.Matricola == this.scafComboBox.SelectedItem.ToString()
                      select s.NumeroCorsia;

            this.corsiaTexBox.Text = res.First().ToString();
        }

        private void posiziona_Click(object sender, EventArgs e)
        {
            if(possoPosizionarlo())
            {
                POSIZIONI p = new POSIZIONI();
                p.EAN = decimal.Parse(this.EANtextBox.Text);
                p.MatricolaScaffale = this.scafComboBox.SelectedItem.ToString();

                db.POSIZIONI.InsertOnSubmit(p);
                db.SubmitChanges();

                caricaLuogoProdotti();
            }
        }

        private bool possoPosizionarlo()
        {
            if(this.EANtextBox.Text != "")
            {
                var res = from p in db.PRODOTTI
                          where p.EAN == decimal.Parse(this.EANtextBox.Text)
                          select p;
                if(res.Count() > 0)
                {
                    if(res.First().StatoInVendita == 'S')
                    {

                        var cc = from s in db.SCAFFALI
                                 join p in db.POSIZIONI on s.Matricola equals p.MatricolaScaffale
                                 where s.NumeroCorsia == decimal.Parse(this.corsiaTexBox.Text)
                                 where p.EAN == decimal.Parse(this.EANtextBox.Text)
                                 select s;
                        if(cc.Count() <= 0)
                        {
                            return true;
                        }
                    }
                }
                            
            }
            return false;
        }
    }
}
