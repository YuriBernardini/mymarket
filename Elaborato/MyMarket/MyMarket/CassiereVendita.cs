﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyMarket
{
    public partial class CassiereVendita : Form
    {
        MyMarketDBDataContext db = new MyMarketDBDataContext();
        decimal scontrinoAttuale;
        decimal idPagSelezionato = 1;

        public CassiereVendita()
        {
            InitializeComponent();
            this.dataLabel.Text = AppData.Instance.DataSelezionata.ToString("dd'/'MM'/'yyyy");
            this.nomeLabel.Text = "CASSIERE: "
                + AppData.Instance.NomeUtenteAttivo + " "
                + AppData.Instance.CognomeUtenteAttivo + " "
                + AppData.Instance.BadgeUtenteAttivo + " CASSA: "
                + AppData.Instance.CassaAttiva;

            this.RiempiPagamento();

            this.NuovoScontrino();
        }

        private void Vendi(object sender, EventArgs e)
        {
            var res = from s in db.SCONTRINI
                      where s.NumeroScontrino == scontrinoAttuale
                      where s.Data == AppData.Instance.DataSelezionata
                      where s.NumeroCassa == AppData.Instance.CassaAttiva
                      select s;

            res.First().IdPagamento = decimal.Parse(this.idPagamento.SelectedItem.ToString());

            db.SubmitChanges();

            this.ResettaTutto();
            NuovoScontrino();
        }

        private void RiempiPagamento()
        {
            var res = from m in db.MODALITA_PAGAMENTO
                      select new
                      {
                          Id = m.Id,
                          Desc = m.Descrizione
                      };
            foreach(var x in res)
            {
                this.idPagamento.Items.Add(x.Id);
            }
            this.idPagamento.SelectedIndex = 0;
        }

        private void ResettaTutto()
        {
            this.EANtextBox.Text = "";
            this.dataGridView1.Rows.Clear();
            this.quantita.Value = 1;
        }

        private void Indietro(object sender, EventArgs e)
        {
            CassiereHome home = new CassiereHome();
            home.Show();
            this.Close();
        }

        private void SoloNumeri(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void InserisciProdotto(object sender, EventArgs e)
        {
            if(ControllaQuantita())
            {
                var pp = from ds in db.DETTAGLI_SCONTRINO
                         where ds.NumeroCassa == AppData.Instance.CassaAttiva
                         where ds.Data.Date == AppData.Instance.DataSelezionata.Date
                         where ds.NumeroScontrino == scontrinoAttuale
                         where ds.EAN == decimal.Parse(this.EANtextBox.Text)
                         select ds;
                //se ce gia
                if(pp.Count() > 0)
                {
                    pp.First().Quantita = pp.First().Quantita + this.quantita.Value;
                }
                else
                {
                    DETTAGLI_SCONTRINO ds = new DETTAGLI_SCONTRINO();
                    ds.Quantita = this.quantita.Value;
                    ds.Data = AppData.Instance.DataSelezionata;
                    ds.NumeroScontrino = this.scontrinoAttuale;
                    ds.NumeroCassa = AppData.Instance.CassaAttiva;
                    ds.EAN = decimal.Parse(this.EANtextBox.Text);

                    db.DETTAGLI_SCONTRINO.InsertOnSubmit(ds);
                }
                db.SubmitChanges();
            }

            AggiornaScontrino();

            this.EANtextBox.Text = "";
        }

        private void AggiornaScontrino()
        {
            var res = from ds in db.DETTAGLI_SCONTRINO
                      join p in db.PRODOTTI on ds.EAN equals p.EAN
                      where ds.NumeroCassa == AppData.Instance.CassaAttiva
                      where ds.Data.Date == AppData.Instance.DataSelezionata.Date
                      where ds.NumeroScontrino == scontrinoAttuale
                      select new
                      {
                          Descrizione = p.Descrizione,
                          EAN = p.EAN,
                          Quantita = ds.Quantita
                      };

            this.dataGridView1.DataSource = res;
                      
        }

        private void NuovoScontrino()
        {
            scontrinoAttuale = AppData.Instance.ScontrinoDaEmettere(AppData.Instance.CassaAttiva);

            this.numeroScontrino.Text = "" + scontrinoAttuale;

            SCONTRINI s = new SCONTRINI();

            s.Data = AppData.Instance.DataSelezionata;
            s.IdPagamento = idPagSelezionato;
            s.NumeroCassa = AppData.Instance.CassaAttiva;
            s.NumeroScontrino = scontrinoAttuale;

            db.SCONTRINI.InsertOnSubmit(s);
            db.SubmitChanges();
        }

        private bool ControllaQuantita()
        {
            if(EANtextBox.Text != "")
            {
                var q = from p in db.PRODOTTI
                        where p.EAN == decimal.Parse(this.EANtextBox.Text)
                        select p;
               
                if (q.Count() > 0)
                {

                    if (q.First().Giacenza >= this.quantita.Value && q.First().StatoInVendita == 'S')
                    {
                        //togli quantità
                        q.First().Giacenza = q.First().Giacenza - this.quantita.Value;

                        db.SubmitChanges();

                        return true;
                    }
                }
            }
            return false;
        }

        private void CaricaPagamenti(object sender, EventArgs e)
        {
            var res = from m in db.MODALITA_PAGAMENTO
                      where m.Id == decimal.Parse(this.idPagamento.SelectedItem.ToString())
                      select m;
            this.pagamento.Text = res.First().Titolo;
        }
    }
}
