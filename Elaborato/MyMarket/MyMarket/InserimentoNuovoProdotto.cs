﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyMarket
{
    public partial class InserimentoNuovoProdotto : Form
    {

        MyMarketDBDataContext db = new MyMarketDBDataContext();

        public InserimentoNuovoProdotto()
        {
            InitializeComponent();

            this.idFornitoreComboBox.Items.Clear();

            var res = from p in db.PRODUTTORI
                      select p;
            foreach(var x in res)
            {
                this.idFornitoreComboBox.Items.Add(x.PartitaIVA);
            }
        }

        private void Inserisci(object sender, EventArgs e)
        {
            this.Controlla();
            this.infoLabel.Visible = true;
        }

        private void Controlla()
        {
            var res = from p in db.PRODOTTI
                      where p.EAN == decimal.Parse(eanTextBox.Text)
                      select p;

            if(res.Count() > 0 || eanTextBox.TextLength > 13)
            {
                this.infoLabel.Text = "ERRORE";
            }
            else
            {
                PRODOTTI p = new PRODOTTI();
                p.EAN = decimal.Parse(eanTextBox.Text);
                p.Descrizione = descTextBox.Text;
                p.PartitaIVA = idFornitoreComboBox.SelectedItem.ToString();
                p.PrezzoAcquisto = float.Parse(prezzoAcquisto.Text);
                p.PrezzoVendita = float.Parse(prezzoVendita.Text);
                p.Giacenza = 50;
                p.StatoInVendita = 'S';

                db.PRODOTTI.InsertOnSubmit(p);
                db.SubmitChanges();

                this.infoLabel.Text = "INSERITO";
                
            }
            
            this.eanTextBox.Clear();
            this.descTextBox.Clear();
            this.prezzoAcquisto.Clear();
            this.prezzoVendita.Clear();

        }
        private void SoloNumeri(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
        }

        private void Indietro(object sender, EventArgs e)
        {
            ScaffalistaHome ind = new ScaffalistaHome();
            ind.Show();
            this.Close();
        }

        private void caricaFornitore(object sender, EventArgs e)
        {
            var res = from p in db.PRODUTTORI
                      where p.PartitaIVA == this.idFornitoreComboBox.SelectedItem.ToString()
                      select p;
            if(res.Count() > 0)
            {
                this.produttoreTextBox.Text = res.First().Intestazione;
            }
        }
    }
}
