﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyMarket
{
    public partial class Home : Form
    {
        AppData dati;

        MyMarketDBDataContext db = new MyMarketDBDataContext();

        string badge;
        string nome;
        string password;
        string ruolo;


        public Home()
        {
            InitializeComponent();
            this.dati = AppData.Instance;
        }

        private void Entra(object sender, EventArgs e)
        {
            this.badge = this.userTextBox.Text;
            this.password = this.pwdTextBox.Text;

            if(this.badge != "" && this.password != "")
            {
                if(this.EsisteAccount())
                {
                    this.CaricaDati();
                    this.ruolo = AppData.Instance.StampaRuolo(this.badge);

                    switch (this.ruolo)
                    {
                        case "SCAFFALISTA":
                            ScaffalistaHome scaffalista = new ScaffalistaHome();
                            scaffalista.Show();
                            break;

                        case "CASSIERE":
                            CassiereHome cassiere = new CassiereHome();
                            cassiere.Show();
                            break;

                        case "RESPONSABILE":
                            ResponsabileHome responsabile = new ResponsabileHome();
                            responsabile.Show();
                            break;
                    }

                    this.Hide();
                }
                else
                {
                    this.userTextBox.Text = "";
                    this.pwdTextBox.Text = "";
                    this.erroreLabel.Visible = true;
                }
            } 
            else
            {
                this.userTextBox.Text = "";
                this.pwdTextBox.Text = "";
                this.erroreLabel.Visible = true;
            }
            
        }

        private void CaricaDati()
        {
            this.dati.EseguiQueryInterna(this.badge, this.nome, this.password, this.ruolo);
        }

        private bool EsisteAccount()
        {
            var res = from p in db.PERSONALE
                      where p.Badge == decimal.Parse(userTextBox.Text) && p.Cognome == pwdTextBox.Text
                      select new { p.Badge, p.Nome, p.Cognome };

            if (res.Count() > 0)
            {
                foreach (var card in res)
                {
                    this.badge = card.Badge.ToString();
                    this.nome = card.Nome;
                    this.password = card.Cognome;
                }
            
                return true;
            }
            else
            {
                return false;
            }
            
        }

        private void NascondiErrore(object sender, EventArgs e)
        {
            this.erroreLabel.Visible = false;
        }

        private void SoloNumeri(object sender, KeyPressEventArgs e)
        {
            this.erroreLabel.Visible = false;
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }
    }
}
