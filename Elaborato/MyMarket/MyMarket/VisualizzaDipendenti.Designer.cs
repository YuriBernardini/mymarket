﻿namespace MyMarket
{
    partial class VisualizzaDipendenti
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VisualizzaDipendenti));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.responsabili = new System.Windows.Forms.Button();
            this.scaffalisti = new System.Windows.Forms.Button();
            this.cassieri = new System.Windows.Forms.Button();
            this.indietroBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.indietroBox)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.Location = new System.Drawing.Point(12, 245);
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.Size = new System.Drawing.Size(1326, 472);
            this.dataGridView1.TabIndex = 1;
            // 
            // responsabili
            // 
            this.responsabili.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.responsabili.Location = new System.Drawing.Point(303, 89);
            this.responsabili.Name = "responsabili";
            this.responsabili.Size = new System.Drawing.Size(196, 61);
            this.responsabili.TabIndex = 2;
            this.responsabili.Text = "RESPONSABILI";
            this.responsabili.UseVisualStyleBackColor = true;
            this.responsabili.Click += new System.EventHandler(this.responsabili_Click);
            // 
            // scaffalisti
            // 
            this.scaffalisti.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scaffalisti.Location = new System.Drawing.Point(843, 89);
            this.scaffalisti.Name = "scaffalisti";
            this.scaffalisti.Size = new System.Drawing.Size(193, 61);
            this.scaffalisti.TabIndex = 5;
            this.scaffalisti.Text = "SCAFFALISTI";
            this.scaffalisti.UseVisualStyleBackColor = true;
            this.scaffalisti.Click += new System.EventHandler(this.scaffalisti_Click);
            // 
            // cassieri
            // 
            this.cassieri.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cassieri.Location = new System.Drawing.Point(579, 89);
            this.cassieri.Name = "cassieri";
            this.cassieri.Size = new System.Drawing.Size(172, 61);
            this.cassieri.TabIndex = 6;
            this.cassieri.Text = "CASSIERI";
            this.cassieri.UseVisualStyleBackColor = true;
            this.cassieri.Click += new System.EventHandler(this.cassieri_Click);
            // 
            // indietroBox
            // 
            this.indietroBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.indietroBox.Image = ((System.Drawing.Image)(resources.GetObject("indietroBox.Image")));
            this.indietroBox.Location = new System.Drawing.Point(12, 12);
            this.indietroBox.Name = "indietroBox";
            this.indietroBox.Size = new System.Drawing.Size(168, 126);
            this.indietroBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.indietroBox.TabIndex = 24;
            this.indietroBox.TabStop = false;
            this.indietroBox.Click += new System.EventHandler(this.Indietro);
            // 
            // VisualizzaDipendenti
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 729);
            this.Controls.Add(this.indietroBox);
            this.Controls.Add(this.cassieri);
            this.Controls.Add(this.scaffalisti);
            this.Controls.Add(this.responsabili);
            this.Controls.Add(this.dataGridView1);
            this.MaximizeBox = false;
            this.Name = "VisualizzaDipendenti";
            this.Text = "VisualizzaDipendenti";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.indietroBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button responsabili;
        private System.Windows.Forms.Button scaffalisti;
        private System.Windows.Forms.Button cassieri;
        private System.Windows.Forms.PictureBox indietroBox;
    }
}