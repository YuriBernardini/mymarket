﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMarket
{
    public sealed class AppData
    {
        //SINGLETON
        private static AppData instanza = null;

        MyMarketDBDataContext db = new MyMarketDBDataContext();

        public decimal UltimoBadge { get; private set; }
        public decimal UltimoMeeting { get; private set; }
        public decimal UltimoIntervento { get; private set; }

        public List<decimal> UltimiScontrini { get; private set; }

        public decimal CassaAttiva { get; set; }

        public string NomeUtenteAttivo { get; private set; }
        public string CognomeUtenteAttivo { get; private set; }
        public string RuoloUtenteAttivo { get; private set; }
        public string BadgeUtenteAttivo { get; private set; }
        public DateTime DataSelezionata { get; set; }
        public bool Fk { get; set; }

        private AppData()
        {
            this.Inizializza();
        }

        private void Inizializza()
        {
            this.DataSelezionata = DateTime.Today;

            var res = from p in db.PERSONALE
                      orderby p.Badge descending
                      select p.Badge;
            this.UltimoBadge = (res.First());

            var res1 = from p in db.MEETING
                       orderby p.Numero descending
                       select p.Numero;
            this.UltimoMeeting = (res1.First());

            var res2 = from p in db.INTERVENTI
                       orderby p.NumeroIntervento descending
                       select p.NumeroIntervento;
            this.UltimoIntervento = (res2.First());

            var casse = from c in db.CASSE
                        orderby c.Numero ascending
                        select c.Numero;

            this.UltimiScontrini = new List<decimal>();

            foreach(var x in casse)
            {
                var n = from s in db.SCONTRINI
                         where s.NumeroCassa == x
                         where s.Data.Date == this.DataSelezionata.Date
                orderby s.NumeroScontrino descending
                         select s.NumeroScontrino;
                
                if (n.Count() > 0)
                    this.UltimiScontrini.Add(n.First());
                else
                    this.UltimiScontrini.Add(0);
            }

        }

        public static AppData Instance
        {
            get
            {
                if (instanza == null)
                {
                    instanza = new AppData();
                }
                return instanza;
            }
        }

        public void EseguiQueryInterna(string badge, string nome, string cognome, string ruolo)
        {
            this.BadgeUtenteAttivo = badge;
            this.NomeUtenteAttivo = nome;
            this.CognomeUtenteAttivo = cognome;
            this.RuoloUtenteAttivo = ruolo;
        }

        public decimal ScontrinoDaEmettere(decimal nCassa)
        {
            if(nCassa <= UltimiScontrini.Count())
            {
                UltimiScontrini[Decimal.ToInt32(nCassa)-1]++;
                return UltimiScontrini[Decimal.ToInt32(nCassa)-1];
            }
            return 0;
        }

        public string StampaRuolo(string numeroBadge)
        {
            var cassiere = from c in db.CASSIERI
                           where c.Badge == decimal.Parse(numeroBadge)
                           select c;
            if (cassiere.Count() > 0)
            {
                return "CASSIERE";
            }
            else
            {
                var scaffalista = from c in db.SCAFFALISTI
                                  where c.Badge == decimal.Parse(numeroBadge)
                                  select c;
                if (scaffalista.Count() > 0)
                {
                    return "SCAFFALISTA";
                }
                else
                {
                    var responsabile = from c in db.RESPONSABILI
                                       where c.Badge == decimal.Parse(numeroBadge)
                                       select c;
                    if (responsabile.Count() > 0)
                    {
                        return "RESPONSABILE";
                    }
                }
            }
            return "";
        }

        public void EliminaRuolo(string badge)
        {
            var cassiere = from c in db.CASSIERI
                           where c.Badge == decimal.Parse(badge)
                           select c;
            if (cassiere.Count() > 0)
            {
                db.CASSIERI.DeleteOnSubmit(cassiere.First());
                db.SubmitChanges();
            }
            else
            {
                var scaffalista = from c in db.SCAFFALISTI
                                  where c.Badge == decimal.Parse(badge)
                                  select c;
                if (scaffalista.Count() > 0)
                {
                    db.SCAFFALISTI.DeleteOnSubmit(scaffalista.First());
                    db.SubmitChanges();
                }
                else
                {
                    var responsabile = from c in db.RESPONSABILI
                                       where c.Badge == decimal.Parse(badge)
                                       select c;
                    if (responsabile.Count() > 0)
                    {
                        try
                        {
                            db.RESPONSABILI.DeleteOnSubmit(responsabile.First());
                            db.SubmitChanges();
                        } catch(Exception e)
                        {
                            Console.WriteLine(e.Message.ToString());
                            this.Fk = true;
                            Console.WriteLine("NON SI PUO ELIMINARE SE HA FK USATE");
                        }
                    }
                }
            }
        }

        public void EliminaPersonale(string badge)
        {
            var res = from c in db.PERSONALE
                      where c.Badge == decimal.Parse(badge)
                      select c;
            try
            {
                db.PERSONALE.DeleteOnSubmit(res.First());
                db.SubmitChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message.ToString());
                this.Fk = true;
                Console.WriteLine("NON SI PUO ELIMINARE SE HA FK USATE");
            }
        }

        public bool EsistePersonale(string numeroBadge)
        {
            if(numeroBadge != "")
            {
                var res = from p in db.PERSONALE
                                      where p.Badge == decimal.Parse(numeroBadge)
                                      select p.Badge;

                            if (res.Count() > 0)
                                return true;
                            else
                                return false;
            }
            return false;
            
        }
        public string StampaNomePersonale(string numeroBadge)
        {
            var res = from p in db.PERSONALE
                      where p.Badge == decimal.Parse(numeroBadge)
                      select p.Nome;

            if (res.Count() > 0)
                return res.First();
            else
                return "";
        }
        public string StampaCognomePersonale(string numeroBadge)
        {
            var res = from p in db.PERSONALE
                      where p.Badge == decimal.Parse(numeroBadge)
                      select p.Cognome;

            if (res.Count() > 0)
                return res.First();
            else
                return "";
        }
        public string StampaDataPersonale(string numeroBadge)
        {
            var res = from p in db.PERSONALE
                      where p.Badge == decimal.Parse(numeroBadge)
                      select p.DataNascita;

            if (res.Count() > 0)
                return res.First().ToString();
            else
                return "";
        }

        public decimal BadgeDaInserire()
        {
            this.UltimoBadge++;
            return this.UltimoBadge;
        }
        public decimal InterventoDaInserire()
        {
            this.UltimoIntervento++;
            return this.UltimoIntervento;
        }

        public decimal MeetingDaInserire()
        {
            this.UltimoMeeting++;
            return this.UltimoMeeting;
        }

        public void InserisciRuolo(decimal badge, string ruolo)
        {
            switch(ruolo)
            {
                case "SCAFFALISTA":
                    SCAFFALISTI s = new SCAFFALISTI
                    {
                        Badge = badge
                    };
                    db.SCAFFALISTI.InsertOnSubmit(s);
                    db.SubmitChanges();

                    break;

                case "CASSIERE":
                    CASSIERI c = new CASSIERI
                    {
                        Badge = badge
                    };
                    db.CASSIERI.InsertOnSubmit(c);
                    db.SubmitChanges();

                    break;

                case "RESPONSABILE":
                    RESPONSABILI r = new RESPONSABILI
                    {
                        Badge = badge
                    };
                    db.RESPONSABILI.InsertOnSubmit(r);
                    db.SubmitChanges();

                    break;
            }
        }

    }
}
