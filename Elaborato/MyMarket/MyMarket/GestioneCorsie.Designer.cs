﻿namespace MyMarket
{
    partial class GestioneCorsie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GestioneCorsie));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.respGridView1 = new System.Windows.Forms.DataGridView();
            this.prodGridView2 = new System.Windows.Forms.DataGridView();
            this.posiziona = new System.Windows.Forms.Button();
            this.EANtextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.scafComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.corsiaTexBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.respGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prodGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(168, 126);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 26;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.Indietro);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(49, 258);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(286, 25);
            this.label1.TabIndex = 28;
            this.label1.Text = "RESPONSABILITA\'  CORSIE";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(559, 258);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(271, 25);
            this.label2.TabIndex = 30;
            this.label2.Text = "DISPOSIZIONE PRODOTTI";
            // 
            // respGridView1
            // 
            this.respGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.respGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.respGridView1.Location = new System.Drawing.Point(12, 286);
            this.respGridView1.Name = "respGridView1";
            this.respGridView1.Size = new System.Drawing.Size(525, 400);
            this.respGridView1.TabIndex = 31;
            // 
            // prodGridView2
            // 
            this.prodGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.prodGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.prodGridView2.Location = new System.Drawing.Point(543, 286);
            this.prodGridView2.Name = "prodGridView2";
            this.prodGridView2.Size = new System.Drawing.Size(795, 400);
            this.prodGridView2.TabIndex = 32;
            // 
            // posiziona
            // 
            this.posiziona.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posiziona.Location = new System.Drawing.Point(796, 119);
            this.posiziona.Name = "posiziona";
            this.posiziona.Size = new System.Drawing.Size(208, 31);
            this.posiziona.TabIndex = 33;
            this.posiziona.Text = "Posiziona";
            this.posiziona.UseVisualStyleBackColor = true;
            this.posiziona.Click += new System.EventHandler(this.posiziona_Click);
            // 
            // EANtextBox
            // 
            this.EANtextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EANtextBox.Location = new System.Drawing.Point(229, 40);
            this.EANtextBox.Name = "EANtextBox";
            this.EANtextBox.Size = new System.Drawing.Size(204, 31);
            this.EANtextBox.TabIndex = 34;
            this.EANtextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SoloNumeri);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(224, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 25);
            this.label3.TabIndex = 35;
            this.label3.Text = "EAN";
            // 
            // scafComboBox
            // 
            this.scafComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.scafComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scafComboBox.FormattingEnabled = true;
            this.scafComboBox.Location = new System.Drawing.Point(229, 117);
            this.scafComboBox.Name = "scafComboBox";
            this.scafComboBox.Size = new System.Drawing.Size(204, 33);
            this.scafComboBox.TabIndex = 36;
            this.scafComboBox.SelectedIndexChanged += new System.EventHandler(this.caricaCorsia);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(224, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(184, 25);
            this.label4.TabIndex = 37;
            this.label4.Text = "Matricola Scaffale";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(517, 91);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 25);
            this.label5.TabIndex = 39;
            this.label5.Text = "Corsia";
            // 
            // corsiaTexBox
            // 
            this.corsiaTexBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.corsiaTexBox.Location = new System.Drawing.Point(522, 119);
            this.corsiaTexBox.Name = "corsiaTexBox";
            this.corsiaTexBox.ReadOnly = true;
            this.corsiaTexBox.Size = new System.Drawing.Size(204, 31);
            this.corsiaTexBox.TabIndex = 38;
            // 
            // GestioneCorsie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 729);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.corsiaTexBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.scafComboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.EANtextBox);
            this.Controls.Add(this.posiziona);
            this.Controls.Add(this.prodGridView2);
            this.Controls.Add(this.respGridView1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.MaximizeBox = false;
            this.Name = "GestioneCorsie";
            this.Text = "GestioneCorsie";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.respGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prodGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView respGridView1;
        private System.Windows.Forms.DataGridView prodGridView2;
        private System.Windows.Forms.Button posiziona;
        private System.Windows.Forms.TextBox EANtextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox scafComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox corsiaTexBox;
    }
}