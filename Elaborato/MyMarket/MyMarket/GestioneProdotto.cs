﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyMarket
{
    public partial class GestioneProdotto : Form
    {
        MyMarketDBDataContext db = new MyMarketDBDataContext();
        public GestioneProdotto()
        {
            InitializeComponent();
        }

        private void Salva(object sender, EventArgs e)
        {
            var res = from p in db.PRODOTTI
                      where p.EAN == decimal.Parse(this.eanTextBox.Text)
                      select p;

            foreach(var x in res)
            {
                if(this.venditaCheckBox.Checked == true)
                {
                    x.StatoInVendita = 'S';
                }
                else
                {
                    x.StatoInVendita = 'N';
                }

                x.PrezzoAcquisto = float.Parse(prezzoAcquisto.Text);
                x.PrezzoVendita = float.Parse(prezzoVendita.Text);

                db.SubmitChanges();
            }

            this.salvaLabel.Visible = true;
        }

        private void CercaProdotto(object sender, EventArgs e)
        {

            var res = from p in db.PRODOTTI
                      join pr in db.PRODUTTORI on p.PartitaIVA equals pr.PartitaIVA
                      where p.EAN == decimal.Parse(this.eanTextBox.Text)
                      select new
                      {
                          pr.Intestazione,
                          p.Descrizione,
                          p.PrezzoVendita,
                          p.PrezzoAcquisto,
                          p.StatoInVendita,
                      };
            if(this.eanTextBox.Text != "")
            {
                if (res.Count() > 0)
                {
                    this.panel.Visible = true;
                    this.descTextBox.Text = res.First().Descrizione;
                    this.prezzoAcquisto.Text = res.First().PrezzoAcquisto.ToString();
                    this.prezzoVendita.Text = res.First().PrezzoVendita.ToString();
                    this.FornitoreTextBox.Text = res.First().Intestazione;
                    if (res.First().StatoInVendita == 'S')
                    {
                        this.venditaCheckBox.Checked = true;
                    }
                    else
                    {
                        this.venditaCheckBox.Checked = false;
                    }
                }
            }
            

        }

        private void Nascondi(object sender, EventArgs e)
        {
            this.salvaLabel.Visible = false;
        }

        private void NascondiPannello(object sender, EventArgs e)
        {
            this.salvaLabel.Visible = false;
            this.panel.Visible = false;
        }

        private void Indietro(object sender, EventArgs e)
        {
            ScaffalistaHome ind = new ScaffalistaHome();
            ind.Show();
            this.Close();
        }

        private void SoloNumeri(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
        }
    }
}
