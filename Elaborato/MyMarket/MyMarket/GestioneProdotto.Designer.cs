﻿namespace MyMarket
{
    partial class GestioneProdotto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GestioneProdotto));
            this.indietroBox = new System.Windows.Forms.PictureBox();
            this.eanTextBox = new System.Windows.Forms.TextBox();
            this.eanLabel = new System.Windows.Forms.Label();
            this.cercaBtn = new System.Windows.Forms.Button();
            this.panel = new System.Windows.Forms.Panel();
            this.salvaLabel = new System.Windows.Forms.Label();
            this.venditaCheckBox = new System.Windows.Forms.CheckBox();
            this.prezzoVendita = new System.Windows.Forms.TextBox();
            this.salvaBtn = new System.Windows.Forms.Button();
            this.venditaLabel = new System.Windows.Forms.Label();
            this.acquistoLabel = new System.Windows.Forms.Label();
            this.fornLabel = new System.Windows.Forms.Label();
            this.descLabel = new System.Windows.Forms.Label();
            this.descTextBox = new System.Windows.Forms.TextBox();
            this.prezzoAcquisto = new System.Windows.Forms.TextBox();
            this.FornitoreTextBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.indietroBox)).BeginInit();
            this.panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // indietroBox
            // 
            this.indietroBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.indietroBox.Image = ((System.Drawing.Image)(resources.GetObject("indietroBox.Image")));
            this.indietroBox.Location = new System.Drawing.Point(12, 12);
            this.indietroBox.Name = "indietroBox";
            this.indietroBox.Size = new System.Drawing.Size(168, 126);
            this.indietroBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.indietroBox.TabIndex = 26;
            this.indietroBox.TabStop = false;
            this.indietroBox.Click += new System.EventHandler(this.Indietro);
            // 
            // eanTextBox
            // 
            this.eanTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eanTextBox.Location = new System.Drawing.Point(238, 107);
            this.eanTextBox.Name = "eanTextBox";
            this.eanTextBox.Size = new System.Drawing.Size(280, 31);
            this.eanTextBox.TabIndex = 27;
            this.eanTextBox.TextChanged += new System.EventHandler(this.NascondiPannello);
            this.eanTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SoloNumeri);
            // 
            // eanLabel
            // 
            this.eanLabel.AutoSize = true;
            this.eanLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eanLabel.Location = new System.Drawing.Point(233, 79);
            this.eanLabel.Name = "eanLabel";
            this.eanLabel.Size = new System.Drawing.Size(55, 25);
            this.eanLabel.TabIndex = 28;
            this.eanLabel.Text = "EAN";
            // 
            // cercaBtn
            // 
            this.cercaBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cercaBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cercaBtn.Location = new System.Drawing.Point(538, 107);
            this.cercaBtn.Name = "cercaBtn";
            this.cercaBtn.Size = new System.Drawing.Size(141, 30);
            this.cercaBtn.TabIndex = 29;
            this.cercaBtn.Text = "CERCA";
            this.cercaBtn.UseVisualStyleBackColor = true;
            this.cercaBtn.Click += new System.EventHandler(this.CercaProdotto);
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.panel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel.Controls.Add(this.salvaLabel);
            this.panel.Controls.Add(this.venditaCheckBox);
            this.panel.Controls.Add(this.prezzoVendita);
            this.panel.Controls.Add(this.salvaBtn);
            this.panel.Controls.Add(this.venditaLabel);
            this.panel.Controls.Add(this.acquistoLabel);
            this.panel.Controls.Add(this.fornLabel);
            this.panel.Controls.Add(this.descLabel);
            this.panel.Controls.Add(this.descTextBox);
            this.panel.Controls.Add(this.prezzoAcquisto);
            this.panel.Controls.Add(this.FornitoreTextBox);
            this.panel.Location = new System.Drawing.Point(-11, 166);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(1471, 575);
            this.panel.TabIndex = 30;
            this.panel.Visible = false;
            // 
            // salvaLabel
            // 
            this.salvaLabel.AutoSize = true;
            this.salvaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salvaLabel.ForeColor = System.Drawing.Color.Red;
            this.salvaLabel.Location = new System.Drawing.Point(391, 224);
            this.salvaLabel.Name = "salvaLabel";
            this.salvaLabel.Size = new System.Drawing.Size(174, 37);
            this.salvaLabel.TabIndex = 23;
            this.salvaLabel.Text = "SALVATO";
            this.salvaLabel.Visible = false;
            // 
            // venditaCheckBox
            // 
            this.venditaCheckBox.AutoSize = true;
            this.venditaCheckBox.Checked = true;
            this.venditaCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.venditaCheckBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.venditaCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.venditaCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.venditaCheckBox.Location = new System.Drawing.Point(21, 352);
            this.venditaCheckBox.Name = "venditaCheckBox";
            this.venditaCheckBox.Size = new System.Drawing.Size(155, 29);
            this.venditaCheckBox.TabIndex = 22;
            this.venditaCheckBox.Text = "IN VENDITA";
            this.venditaCheckBox.UseVisualStyleBackColor = true;
            this.venditaCheckBox.CheckedChanged += new System.EventHandler(this.Nascondi);
            // 
            // prezzoVendita
            // 
            this.prezzoVendita.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prezzoVendita.Location = new System.Drawing.Point(21, 295);
            this.prezzoVendita.Name = "prezzoVendita";
            this.prezzoVendita.Size = new System.Drawing.Size(243, 31);
            this.prezzoVendita.TabIndex = 21;
            this.prezzoVendita.TextChanged += new System.EventHandler(this.Nascondi);
            this.prezzoVendita.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SoloNumeri);
            // 
            // salvaBtn
            // 
            this.salvaBtn.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.salvaBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.salvaBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.salvaBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.salvaBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salvaBtn.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.salvaBtn.Location = new System.Drawing.Point(21, 412);
            this.salvaBtn.Name = "salvaBtn";
            this.salvaBtn.Size = new System.Drawing.Size(243, 34);
            this.salvaBtn.TabIndex = 20;
            this.salvaBtn.Text = "SALVA";
            this.salvaBtn.UseVisualStyleBackColor = false;
            this.salvaBtn.Click += new System.EventHandler(this.Salva);
            // 
            // venditaLabel
            // 
            this.venditaLabel.AutoSize = true;
            this.venditaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.venditaLabel.Location = new System.Drawing.Point(16, 267);
            this.venditaLabel.Name = "venditaLabel";
            this.venditaLabel.Size = new System.Drawing.Size(158, 25);
            this.venditaLabel.TabIndex = 19;
            this.venditaLabel.Text = "Prezzo Vendita";
            // 
            // acquistoLabel
            // 
            this.acquistoLabel.AutoSize = true;
            this.acquistoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.acquistoLabel.Location = new System.Drawing.Point(16, 182);
            this.acquistoLabel.Name = "acquistoLabel";
            this.acquistoLabel.Size = new System.Drawing.Size(168, 25);
            this.acquistoLabel.TabIndex = 17;
            this.acquistoLabel.Text = "Prezzo Acquisto";
            // 
            // fornLabel
            // 
            this.fornLabel.AutoSize = true;
            this.fornLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fornLabel.Location = new System.Drawing.Point(16, 103);
            this.fornLabel.Name = "fornLabel";
            this.fornLabel.Size = new System.Drawing.Size(98, 25);
            this.fornLabel.TabIndex = 16;
            this.fornLabel.Text = "Fornitore";
            // 
            // descLabel
            // 
            this.descLabel.AutoSize = true;
            this.descLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.descLabel.Location = new System.Drawing.Point(16, 28);
            this.descLabel.Name = "descLabel";
            this.descLabel.Size = new System.Drawing.Size(125, 25);
            this.descLabel.TabIndex = 15;
            this.descLabel.Text = "Descrizione";
            // 
            // descTextBox
            // 
            this.descTextBox.Enabled = false;
            this.descTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.descTextBox.Location = new System.Drawing.Point(21, 56);
            this.descTextBox.Name = "descTextBox";
            this.descTextBox.Size = new System.Drawing.Size(243, 31);
            this.descTextBox.TabIndex = 12;
            // 
            // prezzoAcquisto
            // 
            this.prezzoAcquisto.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prezzoAcquisto.Location = new System.Drawing.Point(21, 210);
            this.prezzoAcquisto.Name = "prezzoAcquisto";
            this.prezzoAcquisto.Size = new System.Drawing.Size(243, 31);
            this.prezzoAcquisto.TabIndex = 14;
            this.prezzoAcquisto.TextChanged += new System.EventHandler(this.Nascondi);
            this.prezzoAcquisto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SoloNumeri);
            // 
            // FornitoreTextBox
            // 
            this.FornitoreTextBox.Enabled = false;
            this.FornitoreTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FornitoreTextBox.Location = new System.Drawing.Point(21, 131);
            this.FornitoreTextBox.Name = "FornitoreTextBox";
            this.FornitoreTextBox.Size = new System.Drawing.Size(243, 31);
            this.FornitoreTextBox.TabIndex = 13;
            // 
            // GestioneProdotto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 729);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.cercaBtn);
            this.Controls.Add(this.eanLabel);
            this.Controls.Add(this.eanTextBox);
            this.Controls.Add(this.indietroBox);
            this.MaximizeBox = false;
            this.Name = "GestioneProdotto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GestioneProdotto";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.indietroBox)).EndInit();
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox indietroBox;
        private System.Windows.Forms.TextBox eanTextBox;
        private System.Windows.Forms.Label eanLabel;
        private System.Windows.Forms.Button cercaBtn;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Label salvaLabel;
        private System.Windows.Forms.CheckBox venditaCheckBox;
        private System.Windows.Forms.TextBox prezzoVendita;
        private System.Windows.Forms.Button salvaBtn;
        private System.Windows.Forms.Label venditaLabel;
        private System.Windows.Forms.Label acquistoLabel;
        private System.Windows.Forms.Label fornLabel;
        private System.Windows.Forms.Label descLabel;
        private System.Windows.Forms.TextBox descTextBox;
        private System.Windows.Forms.TextBox prezzoAcquisto;
        private System.Windows.Forms.TextBox FornitoreTextBox;
    }
}