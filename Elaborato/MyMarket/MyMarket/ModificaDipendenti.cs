﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyMarket
{
    public partial class ModificaDipendenti : Form
    {
        MyMarketDBDataContext db = new MyMarketDBDataContext();
        bool fk = false;
        public ModificaDipendenti()
        {
            InitializeComponent();
            this.ruoloComboBox.Items.Clear();
            this.ruoloComboBox.Items.Add("SCAFFALISTA");
            this.ruoloComboBox.Items.Add("CASSIERE");
            this.ruoloComboBox.Items.Add("RESPONSABILE");
        }

        string badge;

        private void Trova(object sender, EventArgs e)
        {
            this.badge = this.BadgeTextBox.Text;

            if (AppData.Instance.EsistePersonale(this.badge))
            {
                this.CompilaCampi();
                this.pannello.Visible = true;
            }
        }

        private void Salva(object sender, EventArgs e)
        {
            this.modificaLabel.Text = "MODIFICATO - " + this.BadgeTextBox.Text;

            AppData.Instance.EliminaRuolo(this.BadgeTextBox.Text);
            
            AppData.Instance.InserisciRuolo(Decimal.Parse(this.BadgeTextBox.Text), this.autoTypeComboBox.SelectedItem.ToString());

            this.modificaLabel.Visible = true;
            this.pannello.Visible = false;
        }

        private void Elimina(object sender, EventArgs e)
        {
            AppData.Instance.EliminaRuolo(this.BadgeTextBox.Text);
            AppData.Instance.EliminaPersonale(this.BadgeTextBox.Text);

            if(AppData.Instance.Fk)
            {
                this.modificaLabel.Text = "IMPOSSIBILE ELIMINARE " + this.BadgeTextBox.Text;
                this.fk = false;
            } else
            {
                this.modificaLabel.Text = "ELIMINATO - " + this.BadgeTextBox.Text;
            }
            this.modificaLabel.Visible = true;
            this.pannello.Visible = false;
        }

        private void CompilaCampi()
        {
            this.autoNameTextBox.Text = AppData.Instance.StampaNomePersonale(this.badge);
            this.autoSurnameTextBox.Text = AppData.Instance.StampaCognomePersonale(this.badge);
            this.autoNascitaTextBox.Text = AppData.Instance.StampaDataPersonale(this.badge);
            this.autoTypeComboBox.Items.Clear();
            this.autoTypeComboBox.Items.Add("SCAFFALISTA");
            this.autoTypeComboBox.Items.Add("CASSIERE");
            this.autoTypeComboBox.Items.Add("RESPONSABILE");
            this.autoTypeComboBox.SelectedItem = AppData.Instance.StampaRuolo(this.badge);
        }

        private void Inserisci(object sender, EventArgs e)
        {
            db.SubmitChanges();

            PERSONALE pers = new PERSONALE
            {
                Badge = AppData.Instance.BadgeDaInserire(),
                Nome = this.nomeTextBox.Text,
                Cognome = this.cognomeTextBox.Text,
                DataNascita = this.dateTimePicker.Value
            };

            db.PERSONALE.InsertOnSubmit(pers);
            db.SubmitChanges();

            AppData.Instance.InserisciRuolo(pers.Badge, ruoloComboBox.SelectedItem.ToString());

            this.aggiuntoLabel.Text = "AGGIUNTO - BADGE: " + pers.Badge;
            this.aggiuntoLabel.Visible = true;

            this.nomeTextBox.Text = "";
            this.cognomeTextBox.Text = "";
            
        }

        private void Nascondi(object sender, EventArgs e)
        {
            this.aggiuntoLabel.Visible = false;
            this.modificaLabel.Visible = false;
            this.pannello.Visible = false;
        }

        private void Indietro(object sender, EventArgs e)
        {
            ResponsabileHome responsabile = new ResponsabileHome();
            responsabile.Show();
            this.Close();
        }

        private void SoloNumeri(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }
    }
}
