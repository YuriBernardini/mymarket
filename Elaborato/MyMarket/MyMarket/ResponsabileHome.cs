﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyMarket
{
    public partial class ResponsabileHome : Form
    {
        public ResponsabileHome()
        {
            InitializeComponent();
            this.ImpostaData();
            this.ImpostaUtente();
            this.dataLabel.Text = AppData.Instance.DataSelezionata.ToString("dd'/'MM'/'yyyy");
        }

        private void Indietro(object sender, EventArgs e)
        {
            Home home = new Home();
            home.Show();
            this.Close();
        }

        private void ImpostaUtente()
        {
            this.infoLabel.Text = "Responsabile: " + AppData.Instance.NomeUtenteAttivo + " " + AppData.Instance.CognomeUtenteAttivo;
        }

        private void ImpostaData()
        {
            this.dataLabel.Text = "" + AppData.Instance.DataSelezionata.Date;
        }

        private void ModificaDipendenti(object sender, EventArgs e)
        {
            ModificaDipendenti modificaDipendenti = new ModificaDipendenti();
            modificaDipendenti.Show();
            this.Close();
        }

        private void Report(object sender, EventArgs e)
        {
            Report report = new Report();
            report.Show();
            this.Close();
        }

        private void visualizzaDipendentiBtn_Click(object sender, EventArgs e)
        {
            VisualizzaDipendenti vd = new VisualizzaDipendenti();
            vd.Show();
            this.Close();
        }

        private void meetingBtn_Click(object sender, EventArgs e)
        {
            Meeting meeting = new Meeting();
            meeting.Show();
            this.Close();
        }
    }
}
