﻿namespace MyMarket
{
    partial class ModificaDipendenti
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModificaDipendenti));
            this.panel = new System.Windows.Forms.Panel();
            this.indietroBox = new System.Windows.Forms.PictureBox();
            this.modificaLabel = new System.Windows.Forms.Label();
            this.pannello = new System.Windows.Forms.Panel();
            this.eliminaButton = new System.Windows.Forms.Button();
            this.modificaButton = new System.Windows.Forms.Button();
            this.ruoloLabel = new System.Windows.Forms.Label();
            this.autoTypeComboBox = new System.Windows.Forms.ComboBox();
            this.nascitaLabel = new System.Windows.Forms.Label();
            this.cognomeLabel = new System.Windows.Forms.Label();
            this.nomLabel = new System.Windows.Forms.Label();
            this.autoNameTextBox = new System.Windows.Forms.TextBox();
            this.autoNascitaTextBox = new System.Windows.Forms.TextBox();
            this.autoSurnameTextBox = new System.Windows.Forms.TextBox();
            this.cercaButton = new System.Windows.Forms.Button();
            this.badgeLabel = new System.Windows.Forms.Label();
            this.infoLabel = new System.Windows.Forms.Label();
            this.BadgeTextBox = new System.Windows.Forms.TextBox();
            this.infoLabel2 = new System.Windows.Forms.Label();
            this.nomeTextBox = new System.Windows.Forms.TextBox();
            this.cognomeTextBox = new System.Windows.Forms.TextBox();
            this.nNomeLabel = new System.Windows.Forms.Label();
            this.nCognomeLabel = new System.Windows.Forms.Label();
            this.nDataLabel = new System.Windows.Forms.Label();
            this.inserisciButton = new System.Windows.Forms.Button();
            this.ruoloComboBox = new System.Windows.Forms.ComboBox();
            this.nRuoloLabel = new System.Windows.Forms.Label();
            this.aggiuntoLabel = new System.Windows.Forms.Label();
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.indietroBox)).BeginInit();
            this.pannello.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel.Controls.Add(this.indietroBox);
            this.panel.Controls.Add(this.modificaLabel);
            this.panel.Controls.Add(this.pannello);
            this.panel.Controls.Add(this.cercaButton);
            this.panel.Controls.Add(this.badgeLabel);
            this.panel.Controls.Add(this.infoLabel);
            this.panel.Controls.Add(this.BadgeTextBox);
            this.panel.Location = new System.Drawing.Point(0, 0);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(683, 768);
            this.panel.TabIndex = 0;
            // 
            // indietroBox
            // 
            this.indietroBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.indietroBox.Image = ((System.Drawing.Image)(resources.GetObject("indietroBox.Image")));
            this.indietroBox.Location = new System.Drawing.Point(11, 9);
            this.indietroBox.Name = "indietroBox";
            this.indietroBox.Size = new System.Drawing.Size(168, 126);
            this.indietroBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.indietroBox.TabIndex = 23;
            this.indietroBox.TabStop = false;
            this.indietroBox.Click += new System.EventHandler(this.Indietro);
            // 
            // modificaLabel
            // 
            this.modificaLabel.AutoSize = true;
            this.modificaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modificaLabel.ForeColor = System.Drawing.Color.Red;
            this.modificaLabel.Location = new System.Drawing.Point(191, 616);
            this.modificaLabel.Name = "modificaLabel";
            this.modificaLabel.Size = new System.Drawing.Size(354, 31);
            this.modificaLabel.TabIndex = 12;
            this.modificaLabel.Text = "ELIMINATO - BADGE: 0004";
            this.modificaLabel.Visible = false;
            // 
            // pannello
            // 
            this.pannello.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pannello.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pannello.Controls.Add(this.eliminaButton);
            this.pannello.Controls.Add(this.modificaButton);
            this.pannello.Controls.Add(this.ruoloLabel);
            this.pannello.Controls.Add(this.autoTypeComboBox);
            this.pannello.Controls.Add(this.nascitaLabel);
            this.pannello.Controls.Add(this.cognomeLabel);
            this.pannello.Controls.Add(this.nomLabel);
            this.pannello.Controls.Add(this.autoNameTextBox);
            this.pannello.Controls.Add(this.autoNascitaTextBox);
            this.pannello.Controls.Add(this.autoSurnameTextBox);
            this.pannello.Location = new System.Drawing.Point(3, 253);
            this.pannello.Name = "pannello";
            this.pannello.Size = new System.Drawing.Size(675, 350);
            this.pannello.TabIndex = 22;
            this.pannello.Visible = false;
            // 
            // eliminaButton
            // 
            this.eliminaButton.BackColor = System.Drawing.SystemColors.Control;
            this.eliminaButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.eliminaButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.eliminaButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.eliminaButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eliminaButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.eliminaButton.Location = new System.Drawing.Point(370, 240);
            this.eliminaButton.Name = "eliminaButton";
            this.eliminaButton.Size = new System.Drawing.Size(243, 34);
            this.eliminaButton.TabIndex = 21;
            this.eliminaButton.Text = "ELIMINA";
            this.eliminaButton.UseVisualStyleBackColor = false;
            this.eliminaButton.Click += new System.EventHandler(this.Elimina);
            // 
            // modificaButton
            // 
            this.modificaButton.BackColor = System.Drawing.SystemColors.Control;
            this.modificaButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.modificaButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.modificaButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.modificaButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modificaButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.modificaButton.Location = new System.Drawing.Point(370, 128);
            this.modificaButton.Name = "modificaButton";
            this.modificaButton.Size = new System.Drawing.Size(243, 34);
            this.modificaButton.TabIndex = 20;
            this.modificaButton.Text = "MODIFICA DATI";
            this.modificaButton.UseVisualStyleBackColor = false;
            this.modificaButton.Click += new System.EventHandler(this.Salva);
            // 
            // ruoloLabel
            // 
            this.ruoloLabel.AutoSize = true;
            this.ruoloLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ruoloLabel.Location = new System.Drawing.Point(7, 240);
            this.ruoloLabel.Name = "ruoloLabel";
            this.ruoloLabel.Size = new System.Drawing.Size(68, 25);
            this.ruoloLabel.TabIndex = 19;
            this.ruoloLabel.Text = "Ruolo";
            // 
            // autoTypeComboBox
            // 
            this.autoTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.autoTypeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.autoTypeComboBox.FormattingEnabled = true;
            this.autoTypeComboBox.Location = new System.Drawing.Point(8, 268);
            this.autoTypeComboBox.Name = "autoTypeComboBox";
            this.autoTypeComboBox.Size = new System.Drawing.Size(243, 33);
            this.autoTypeComboBox.TabIndex = 18;
            // 
            // nascitaLabel
            // 
            this.nascitaLabel.AutoSize = true;
            this.nascitaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nascitaLabel.Location = new System.Drawing.Point(7, 165);
            this.nascitaLabel.Name = "nascitaLabel";
            this.nascitaLabel.Size = new System.Drawing.Size(158, 25);
            this.nascitaLabel.TabIndex = 17;
            this.nascitaLabel.Text = "Data di Nascita";
            // 
            // cognomeLabel
            // 
            this.cognomeLabel.AutoSize = true;
            this.cognomeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cognomeLabel.Location = new System.Drawing.Point(3, 96);
            this.cognomeLabel.Name = "cognomeLabel";
            this.cognomeLabel.Size = new System.Drawing.Size(104, 25);
            this.cognomeLabel.TabIndex = 16;
            this.cognomeLabel.Text = "Cognome";
            // 
            // nomLabel
            // 
            this.nomLabel.AutoSize = true;
            this.nomLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nomLabel.Location = new System.Drawing.Point(3, 25);
            this.nomLabel.Name = "nomLabel";
            this.nomLabel.Size = new System.Drawing.Size(68, 25);
            this.nomLabel.TabIndex = 15;
            this.nomLabel.Text = "Nome";
            // 
            // autoNameTextBox
            // 
            this.autoNameTextBox.Enabled = false;
            this.autoNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.autoNameTextBox.Location = new System.Drawing.Point(8, 53);
            this.autoNameTextBox.Name = "autoNameTextBox";
            this.autoNameTextBox.Size = new System.Drawing.Size(243, 31);
            this.autoNameTextBox.TabIndex = 12;
            // 
            // autoNascitaTextBox
            // 
            this.autoNascitaTextBox.Enabled = false;
            this.autoNascitaTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.autoNascitaTextBox.Location = new System.Drawing.Point(6, 198);
            this.autoNascitaTextBox.Name = "autoNascitaTextBox";
            this.autoNascitaTextBox.Size = new System.Drawing.Size(243, 31);
            this.autoNascitaTextBox.TabIndex = 14;
            // 
            // autoSurnameTextBox
            // 
            this.autoSurnameTextBox.Enabled = false;
            this.autoSurnameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.autoSurnameTextBox.Location = new System.Drawing.Point(8, 128);
            this.autoSurnameTextBox.Name = "autoSurnameTextBox";
            this.autoSurnameTextBox.Size = new System.Drawing.Size(243, 31);
            this.autoSurnameTextBox.TabIndex = 13;
            // 
            // cercaButton
            // 
            this.cercaButton.BackColor = System.Drawing.SystemColors.Control;
            this.cercaButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cercaButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.cercaButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cercaButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cercaButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.cercaButton.Location = new System.Drawing.Point(378, 163);
            this.cercaButton.Name = "cercaButton";
            this.cercaButton.Size = new System.Drawing.Size(243, 34);
            this.cercaButton.TabIndex = 14;
            this.cercaButton.Text = "RICERCA";
            this.cercaButton.UseVisualStyleBackColor = false;
            this.cercaButton.Click += new System.EventHandler(this.Trova);
            // 
            // badgeLabel
            // 
            this.badgeLabel.AutoSize = true;
            this.badgeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.badgeLabel.Location = new System.Drawing.Point(12, 138);
            this.badgeLabel.Name = "badgeLabel";
            this.badgeLabel.Size = new System.Drawing.Size(74, 25);
            this.badgeLabel.TabIndex = 13;
            this.badgeLabel.Text = "Badge";
            // 
            // infoLabel
            // 
            this.infoLabel.AutoSize = true;
            this.infoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.infoLabel.ForeColor = System.Drawing.Color.Black;
            this.infoLabel.Location = new System.Drawing.Point(185, 34);
            this.infoLabel.Name = "infoLabel";
            this.infoLabel.Size = new System.Drawing.Size(193, 39);
            this.infoLabel.TabIndex = 0;
            this.infoLabel.Text = "MODIFICA";
            // 
            // BadgeTextBox
            // 
            this.BadgeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BadgeTextBox.Location = new System.Drawing.Point(12, 166);
            this.BadgeTextBox.Name = "BadgeTextBox";
            this.BadgeTextBox.Size = new System.Drawing.Size(243, 31);
            this.BadgeTextBox.TabIndex = 12;
            this.BadgeTextBox.TextChanged += new System.EventHandler(this.Nascondi);
            this.BadgeTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SoloNumeri);
            // 
            // infoLabel2
            // 
            this.infoLabel2.AutoSize = true;
            this.infoLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.infoLabel2.ForeColor = System.Drawing.Color.Black;
            this.infoLabel2.Location = new System.Drawing.Point(874, 35);
            this.infoLabel2.Name = "infoLabel2";
            this.infoLabel2.Size = new System.Drawing.Size(339, 39);
            this.infoLabel2.TabIndex = 1;
            this.infoLabel2.Text = "INSERISCI NUOVO";
            // 
            // nomeTextBox
            // 
            this.nomeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nomeTextBox.Location = new System.Drawing.Point(689, 166);
            this.nomeTextBox.Name = "nomeTextBox";
            this.nomeTextBox.Size = new System.Drawing.Size(243, 31);
            this.nomeTextBox.TabIndex = 2;
            this.nomeTextBox.TextChanged += new System.EventHandler(this.Nascondi);
            // 
            // cognomeTextBox
            // 
            this.cognomeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cognomeTextBox.Location = new System.Drawing.Point(689, 237);
            this.cognomeTextBox.Name = "cognomeTextBox";
            this.cognomeTextBox.Size = new System.Drawing.Size(243, 31);
            this.cognomeTextBox.TabIndex = 3;
            this.cognomeTextBox.TextChanged += new System.EventHandler(this.Nascondi);
            // 
            // nNomeLabel
            // 
            this.nNomeLabel.AutoSize = true;
            this.nNomeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nNomeLabel.Location = new System.Drawing.Point(689, 138);
            this.nNomeLabel.Name = "nNomeLabel";
            this.nNomeLabel.Size = new System.Drawing.Size(68, 25);
            this.nNomeLabel.TabIndex = 5;
            this.nNomeLabel.Text = "Nome";
            // 
            // nCognomeLabel
            // 
            this.nCognomeLabel.AutoSize = true;
            this.nCognomeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nCognomeLabel.Location = new System.Drawing.Point(689, 209);
            this.nCognomeLabel.Name = "nCognomeLabel";
            this.nCognomeLabel.Size = new System.Drawing.Size(104, 25);
            this.nCognomeLabel.TabIndex = 6;
            this.nCognomeLabel.Text = "Cognome";
            // 
            // nDataLabel
            // 
            this.nDataLabel.AutoSize = true;
            this.nDataLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nDataLabel.Location = new System.Drawing.Point(689, 278);
            this.nDataLabel.Name = "nDataLabel";
            this.nDataLabel.Size = new System.Drawing.Size(158, 25);
            this.nDataLabel.TabIndex = 7;
            this.nDataLabel.Text = "Data di Nascita";
            // 
            // inserisciButton
            // 
            this.inserisciButton.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.inserisciButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.inserisciButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.inserisciButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.inserisciButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inserisciButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.inserisciButton.Location = new System.Drawing.Point(1055, 234);
            this.inserisciButton.Name = "inserisciButton";
            this.inserisciButton.Size = new System.Drawing.Size(243, 34);
            this.inserisciButton.TabIndex = 8;
            this.inserisciButton.Text = "INSERISCI";
            this.inserisciButton.UseVisualStyleBackColor = false;
            this.inserisciButton.Click += new System.EventHandler(this.Inserisci);
            // 
            // ruoloComboBox
            // 
            this.ruoloComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ruoloComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ruoloComboBox.FormattingEnabled = true;
            this.ruoloComboBox.Location = new System.Drawing.Point(689, 381);
            this.ruoloComboBox.Name = "ruoloComboBox";
            this.ruoloComboBox.Size = new System.Drawing.Size(243, 33);
            this.ruoloComboBox.TabIndex = 9;
            this.ruoloComboBox.TextChanged += new System.EventHandler(this.Nascondi);
            // 
            // nRuoloLabel
            // 
            this.nRuoloLabel.AutoSize = true;
            this.nRuoloLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nRuoloLabel.Location = new System.Drawing.Point(685, 353);
            this.nRuoloLabel.Name = "nRuoloLabel";
            this.nRuoloLabel.Size = new System.Drawing.Size(68, 25);
            this.nRuoloLabel.TabIndex = 10;
            this.nRuoloLabel.Text = "Ruolo";
            // 
            // aggiuntoLabel
            // 
            this.aggiuntoLabel.AutoSize = true;
            this.aggiuntoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aggiuntoLabel.ForeColor = System.Drawing.Color.Red;
            this.aggiuntoLabel.Location = new System.Drawing.Point(689, 451);
            this.aggiuntoLabel.Name = "aggiuntoLabel";
            this.aggiuntoLabel.Size = new System.Drawing.Size(353, 31);
            this.aggiuntoLabel.TabIndex = 11;
            this.aggiuntoLabel.Text = "AGGIUNTO - BADGE: 0004";
            this.aggiuntoLabel.Visible = false;
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dateTimePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker.Location = new System.Drawing.Point(689, 309);
            this.dateTimePicker.MaxDate = new System.DateTime(2019, 6, 15, 0, 0, 0, 0);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(243, 31);
            this.dateTimePicker.TabIndex = 12;
            this.dateTimePicker.Value = new System.DateTime(2019, 6, 15, 0, 0, 0, 0);
            // 
            // ModificaDipendenti
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 729);
            this.Controls.Add(this.dateTimePicker);
            this.Controls.Add(this.aggiuntoLabel);
            this.Controls.Add(this.nRuoloLabel);
            this.Controls.Add(this.ruoloComboBox);
            this.Controls.Add(this.inserisciButton);
            this.Controls.Add(this.nDataLabel);
            this.Controls.Add(this.nCognomeLabel);
            this.Controls.Add(this.nNomeLabel);
            this.Controls.Add(this.cognomeTextBox);
            this.Controls.Add(this.nomeTextBox);
            this.Controls.Add(this.infoLabel2);
            this.Controls.Add(this.panel);
            this.MaximizeBox = false;
            this.Name = "ModificaDipendenti";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ModificaDipendenti";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.indietroBox)).EndInit();
            this.pannello.ResumeLayout(false);
            this.pannello.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Label infoLabel;
        private System.Windows.Forms.Label infoLabel2;
        private System.Windows.Forms.TextBox nomeTextBox;
        private System.Windows.Forms.TextBox cognomeTextBox;
        private System.Windows.Forms.Label nNomeLabel;
        private System.Windows.Forms.Label nCognomeLabel;
        private System.Windows.Forms.Label nDataLabel;
        private System.Windows.Forms.Button inserisciButton;
        private System.Windows.Forms.ComboBox ruoloComboBox;
        private System.Windows.Forms.Label nRuoloLabel;
        private System.Windows.Forms.Label aggiuntoLabel;
        private System.Windows.Forms.Button eliminaButton;
        private System.Windows.Forms.Button modificaButton;
        private System.Windows.Forms.Label ruoloLabel;
        private System.Windows.Forms.Button cercaButton;
        private System.Windows.Forms.ComboBox autoTypeComboBox;
        private System.Windows.Forms.Label badgeLabel;
        private System.Windows.Forms.Label nascitaLabel;
        private System.Windows.Forms.Label cognomeLabel;
        private System.Windows.Forms.TextBox BadgeTextBox;
        private System.Windows.Forms.Label nomLabel;
        private System.Windows.Forms.TextBox autoNameTextBox;
        private System.Windows.Forms.TextBox autoSurnameTextBox;
        private System.Windows.Forms.Panel pannello;
        private System.Windows.Forms.Label modificaLabel;
        private System.Windows.Forms.PictureBox indietroBox;
        private System.Windows.Forms.DateTimePicker dateTimePicker;
        private System.Windows.Forms.TextBox autoNascitaTextBox;
    }
}