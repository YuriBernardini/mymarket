﻿namespace MyMarket
{
    partial class InserimentoNuovoProdotto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InserimentoNuovoProdotto));
            this.prezzoVendita = new System.Windows.Forms.TextBox();
            this.venditaLabel = new System.Windows.Forms.Label();
            this.acquistoLabel = new System.Windows.Forms.Label();
            this.fornLabel = new System.Windows.Forms.Label();
            this.descLabel = new System.Windows.Forms.Label();
            this.descTextBox = new System.Windows.Forms.TextBox();
            this.prezzoAcquisto = new System.Windows.Forms.TextBox();
            this.eanLabel = new System.Windows.Forms.Label();
            this.eanTextBox = new System.Windows.Forms.TextBox();
            this.inserisciBtn = new System.Windows.Forms.Button();
            this.infoLabel = new System.Windows.Forms.Label();
            this.titolo = new System.Windows.Forms.Label();
            this.indietroBox = new System.Windows.Forms.PictureBox();
            this.idFornitoreComboBox = new System.Windows.Forms.ComboBox();
            this.produttoreTextBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.indietroBox)).BeginInit();
            this.SuspendLayout();
            // 
            // prezzoVendita
            // 
            this.prezzoVendita.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prezzoVendita.Location = new System.Drawing.Point(524, 482);
            this.prezzoVendita.Name = "prezzoVendita";
            this.prezzoVendita.Size = new System.Drawing.Size(320, 31);
            this.prezzoVendita.TabIndex = 35;
            this.prezzoVendita.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SoloNumeri);
            // 
            // venditaLabel
            // 
            this.venditaLabel.AutoSize = true;
            this.venditaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.venditaLabel.Location = new System.Drawing.Point(519, 454);
            this.venditaLabel.Name = "venditaLabel";
            this.venditaLabel.Size = new System.Drawing.Size(158, 25);
            this.venditaLabel.TabIndex = 34;
            this.venditaLabel.Text = "Prezzo Vendita";
            // 
            // acquistoLabel
            // 
            this.acquistoLabel.AutoSize = true;
            this.acquistoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.acquistoLabel.Location = new System.Drawing.Point(519, 377);
            this.acquistoLabel.Name = "acquistoLabel";
            this.acquistoLabel.Size = new System.Drawing.Size(168, 25);
            this.acquistoLabel.TabIndex = 33;
            this.acquistoLabel.Text = "Prezzo Acquisto";
            // 
            // fornLabel
            // 
            this.fornLabel.AutoSize = true;
            this.fornLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fornLabel.Location = new System.Drawing.Point(519, 263);
            this.fornLabel.Name = "fornLabel";
            this.fornLabel.Size = new System.Drawing.Size(112, 25);
            this.fornLabel.TabIndex = 32;
            this.fornLabel.Text = "Produttore";
            // 
            // descLabel
            // 
            this.descLabel.AutoSize = true;
            this.descLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.descLabel.Location = new System.Drawing.Point(519, 186);
            this.descLabel.Name = "descLabel";
            this.descLabel.Size = new System.Drawing.Size(125, 25);
            this.descLabel.TabIndex = 31;
            this.descLabel.Text = "Descrizione";
            // 
            // descTextBox
            // 
            this.descTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.descTextBox.Location = new System.Drawing.Point(524, 214);
            this.descTextBox.Name = "descTextBox";
            this.descTextBox.Size = new System.Drawing.Size(320, 31);
            this.descTextBox.TabIndex = 28;
            // 
            // prezzoAcquisto
            // 
            this.prezzoAcquisto.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prezzoAcquisto.Location = new System.Drawing.Point(524, 405);
            this.prezzoAcquisto.Name = "prezzoAcquisto";
            this.prezzoAcquisto.Size = new System.Drawing.Size(320, 31);
            this.prezzoAcquisto.TabIndex = 30;
            this.prezzoAcquisto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SoloNumeri);
            // 
            // eanLabel
            // 
            this.eanLabel.AutoSize = true;
            this.eanLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eanLabel.Location = new System.Drawing.Point(519, 112);
            this.eanLabel.Name = "eanLabel";
            this.eanLabel.Size = new System.Drawing.Size(55, 25);
            this.eanLabel.TabIndex = 37;
            this.eanLabel.Text = "EAN";
            // 
            // eanTextBox
            // 
            this.eanTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eanTextBox.Location = new System.Drawing.Point(524, 140);
            this.eanTextBox.Name = "eanTextBox";
            this.eanTextBox.Size = new System.Drawing.Size(320, 31);
            this.eanTextBox.TabIndex = 36;
            this.eanTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SoloNumeri);
            // 
            // inserisciBtn
            // 
            this.inserisciBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.inserisciBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.inserisciBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inserisciBtn.Location = new System.Drawing.Point(556, 548);
            this.inserisciBtn.Name = "inserisciBtn";
            this.inserisciBtn.Size = new System.Drawing.Size(243, 64);
            this.inserisciBtn.TabIndex = 39;
            this.inserisciBtn.Text = "INSERISCI";
            this.inserisciBtn.UseVisualStyleBackColor = true;
            this.inserisciBtn.Click += new System.EventHandler(this.Inserisci);
            // 
            // infoLabel
            // 
            this.infoLabel.AutoSize = true;
            this.infoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.infoLabel.ForeColor = System.Drawing.Color.Red;
            this.infoLabel.Location = new System.Drawing.Point(583, 621);
            this.infoLabel.Name = "infoLabel";
            this.infoLabel.Size = new System.Drawing.Size(200, 42);
            this.infoLabel.TabIndex = 40;
            this.infoLabel.Text = "INSERITO";
            this.infoLabel.Visible = false;
            // 
            // titolo
            // 
            this.titolo.AutoSize = true;
            this.titolo.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titolo.Location = new System.Drawing.Point(242, 62);
            this.titolo.Name = "titolo";
            this.titolo.Size = new System.Drawing.Size(1007, 39);
            this.titolo.TabIndex = 41;
            this.titolo.Text = "INSERISCI NUOVO PRODOTTO DA METTERE IN VENDITA";
            // 
            // indietroBox
            // 
            this.indietroBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.indietroBox.Image = ((System.Drawing.Image)(resources.GetObject("indietroBox.Image")));
            this.indietroBox.Location = new System.Drawing.Point(12, 12);
            this.indietroBox.Name = "indietroBox";
            this.indietroBox.Size = new System.Drawing.Size(168, 126);
            this.indietroBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.indietroBox.TabIndex = 42;
            this.indietroBox.TabStop = false;
            this.indietroBox.Click += new System.EventHandler(this.Indietro);
            // 
            // idFornitoreComboBox
            // 
            this.idFornitoreComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.idFornitoreComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idFornitoreComboBox.FormattingEnabled = true;
            this.idFornitoreComboBox.Location = new System.Drawing.Point(524, 291);
            this.idFornitoreComboBox.Name = "idFornitoreComboBox";
            this.idFornitoreComboBox.Size = new System.Drawing.Size(320, 33);
            this.idFornitoreComboBox.TabIndex = 43;
            this.idFornitoreComboBox.SelectedIndexChanged += new System.EventHandler(this.caricaFornitore);
            // 
            // produttoreTextBox
            // 
            this.produttoreTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.produttoreTextBox.Location = new System.Drawing.Point(524, 343);
            this.produttoreTextBox.Name = "produttoreTextBox";
            this.produttoreTextBox.ReadOnly = true;
            this.produttoreTextBox.Size = new System.Drawing.Size(320, 31);
            this.produttoreTextBox.TabIndex = 44;
            // 
            // InserimentoNuovoProdotto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 729);
            this.Controls.Add(this.produttoreTextBox);
            this.Controls.Add(this.idFornitoreComboBox);
            this.Controls.Add(this.indietroBox);
            this.Controls.Add(this.titolo);
            this.Controls.Add(this.infoLabel);
            this.Controls.Add(this.inserisciBtn);
            this.Controls.Add(this.eanLabel);
            this.Controls.Add(this.eanTextBox);
            this.Controls.Add(this.prezzoVendita);
            this.Controls.Add(this.venditaLabel);
            this.Controls.Add(this.acquistoLabel);
            this.Controls.Add(this.fornLabel);
            this.Controls.Add(this.descLabel);
            this.Controls.Add(this.descTextBox);
            this.Controls.Add(this.prezzoAcquisto);
            this.MaximizeBox = false;
            this.Name = "InserimentoNuovoProdotto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "InserimentoNuovoProdotto";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.indietroBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox prezzoVendita;
        private System.Windows.Forms.Label venditaLabel;
        private System.Windows.Forms.Label acquistoLabel;
        private System.Windows.Forms.Label fornLabel;
        private System.Windows.Forms.Label descLabel;
        private System.Windows.Forms.TextBox descTextBox;
        private System.Windows.Forms.TextBox prezzoAcquisto;
        private System.Windows.Forms.Label eanLabel;
        private System.Windows.Forms.TextBox eanTextBox;
        private System.Windows.Forms.Button inserisciBtn;
        private System.Windows.Forms.Label infoLabel;
        private System.Windows.Forms.Label titolo;
        private System.Windows.Forms.PictureBox indietroBox;
        private System.Windows.Forms.ComboBox idFornitoreComboBox;
        private System.Windows.Forms.TextBox produttoreTextBox;
    }
}