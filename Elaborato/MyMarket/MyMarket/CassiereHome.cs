﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyMarket
{
    public partial class CassiereHome : Form
    {
        MyMarketDBDataContext db = new MyMarketDBDataContext();

        public CassiereHome()
        {
            InitializeComponent();
            this.dataLabel.Text = AppData.Instance.DataSelezionata.ToString("dd'/'MM'/'yyyy");
            this.nomeLabel.Text = "CASSIERE: "
                + AppData.Instance.NomeUtenteAttivo + " "
                + AppData.Instance.CognomeUtenteAttivo + " "
                + AppData.Instance.BadgeUtenteAttivo;
            CaricaInterventi();
            AggiornaCasse();
        }

        private void Indietro(object sender, EventArgs e)
        {
            Home home = new Home();
            home.Show();
            this.Close();
        }

        private void AggiornaCasse()
        {
            var res = from c in db.CASSE
                      orderby c.Numero 
                      select c.Numero;
            foreach(var x in res)
            {
                cassaAccesso.Items.Add(x.ToString());
                interventoCassa.Items.Add(x.ToString());
            }
            cassaAccesso.SelectedIndex = 0;
            interventoCassa.SelectedIndex = 0;
        }

        private void CaricaInterventi()
        {
            var res = from i in db.INTERVENTI
                      select new
                      {
                          NumeroIntervento = i.NumeroIntervento,
                          NumeroCassa = i.Numero,
                          Durata = i.Durata,
                          Specialista = i.Specialista,
                          Causale = i.Causale,
                          Problemi = i.Problemi,
                          Data = i.Data,
                          Descrizione = i.Descrizione
                      };
            this.dataGridView1.DataSource = res;
        }

        private void aggiungi_Click(object sender, EventArgs e)
        {
            if(CampiOk())
            {
                INTERVENTI i = new INTERVENTI();
                i.Numero = Decimal.Parse(interventoCassa.SelectedItem.ToString());
                i.Data = AppData.Instance.DataSelezionata;
                i.Causale = causale.Text;
                i.Descrizione = descrizione.Text;
                i.Durata = durata.Text;
                i.NumeroIntervento = AppData.Instance.InterventoDaInserire();
                i.Specialista = specialista.Text;
                i.Problemi = problemi.Text;

                db.INTERVENTI.InsertOnSubmit(i);
                db.SubmitChanges();
                
            }

            this.CaricaInterventi();
        }

        private void apri_Click(object sender, EventArgs e)
        {
            //ho gia aperto altra cassa senza chiuderla
            var res = from a in db.ACCESSI_CASSE
                      where a.IdPersonale == decimal.Parse(AppData.Instance.BadgeUtenteAttivo)
                      where a.OraLogout == null
                      select a;

            if(res.Count() > 0)
            {
                this.errore.Text = "chiudere cassa";
                this.errore.Visible = true;
            }
            else
            {
                //qualcuno ha aperto questa cassa e non l'ha chiusa
                var aperta = from a in db.ACCESSI_CASSE
                             where a.Numero == decimal.Parse(cassaAccesso.SelectedItem.ToString())
                             where a.OraLogout == null
                             select a;
                if (aperta.Count() > 0)
                {
                    this.errore.Text = "cassa aperta";
                    this.errore.Visible = true;
                }
                else
                {
                    ACCESSI_CASSE ac = new ACCESSI_CASSE();
                    ac.Data = AppData.Instance.DataSelezionata;
                    ac.IdPersonale = Decimal.Parse(AppData.Instance.BadgeUtenteAttivo);
                    var orario = DateTime.Now;
                    ac.OraLogin = decimal.Parse(orario.Hour.ToString());
                    ac.Numero = Decimal.Parse(cassaAccesso.SelectedItem.ToString());
                    ac.OraLogout = null;

                    try
                    {

                        db.ACCESSI_CASSE.InsertOnSubmit(ac);
                        db.SubmitChanges();

                        AppData.Instance.CassaAttiva = decimal.Parse(this.cassaAccesso.SelectedItem.ToString());

                        CassiereVendita cv = new CassiereVendita();
                        cv.Show();
                        this.Close();
                    }
                    catch (Exception er)
                    {
                        this.errore.Text = "errore apertura";
                        Console.WriteLine(er.Message.ToString());
                        this.errore.Visible = true;
                    }
                }
            }
        }

        private void chiudi_Click(object sender, EventArgs e)
        {
            var res = from ac in db.ACCESSI_CASSE
                      where ac.IdPersonale == decimal.Parse(AppData.Instance.BadgeUtenteAttivo) 
                      where ac.Data == AppData.Instance.DataSelezionata
                      where ac.Numero == decimal.Parse(cassaAccesso.SelectedItem.ToString())
                      where ac.OraLogout == null
                      select ac;
            foreach(var x in res)
            {
                var orario = DateTime.Now;
                x.OraLogout = decimal.Parse(orario.Hour.ToString());
                db.SubmitChanges();

                this.errore.Text = "chiusa";
                this.errore.Visible = true;
            }
        }

        private bool CampiOk()
        {
            if (interventoCassa.SelectedItem.ToString() == "")
                return false;
            if (durata.Text == "")
                return false;
            if (descrizione.Text == "")
                return false;
            if (causale.Text == "")
                return false;
            if (problemi.Text == "")
                return false;
            if (specialista.Text == "")
                return false;
            return true;
        }

        private void Nascondi(object sender, EventArgs e)
        {
            this.errore.Visible = false;
        }
    }
}
