﻿namespace MyMarket
{
    partial class CassiereHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CassiereHome));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.indietroBox = new System.Windows.Forms.PictureBox();
            this.dataLabel = new System.Windows.Forms.Label();
            this.nomeLabel = new System.Windows.Forms.Label();
            this.cassaAccesso = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.apri = new System.Windows.Forms.Button();
            this.chiudi = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.interventoCassa = new System.Windows.Forms.ComboBox();
            this.durata = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.specialista = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.descrizione = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.causale = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.problemi = new System.Windows.Forms.TextBox();
            this.aggiungi = new System.Windows.Forms.Button();
            this.errore = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.indietroBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // indietroBox
            // 
            this.indietroBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.indietroBox.Image = ((System.Drawing.Image)(resources.GetObject("indietroBox.Image")));
            this.indietroBox.Location = new System.Drawing.Point(12, 12);
            this.indietroBox.Name = "indietroBox";
            this.indietroBox.Size = new System.Drawing.Size(168, 126);
            this.indietroBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.indietroBox.TabIndex = 25;
            this.indietroBox.TabStop = false;
            this.indietroBox.Click += new System.EventHandler(this.Indietro);
            // 
            // dataLabel
            // 
            this.dataLabel.AutoSize = true;
            this.dataLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataLabel.Location = new System.Drawing.Point(196, 12);
            this.dataLabel.Name = "dataLabel";
            this.dataLabel.Size = new System.Drawing.Size(120, 25);
            this.dataLabel.TabIndex = 33;
            this.dataLabel.Text = "01/02/1998";
            // 
            // nomeLabel
            // 
            this.nomeLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.nomeLabel.AutoSize = true;
            this.nomeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nomeLabel.ForeColor = System.Drawing.Color.Black;
            this.nomeLabel.Location = new System.Drawing.Point(320, 96);
            this.nomeLabel.Name = "nomeLabel";
            this.nomeLabel.Size = new System.Drawing.Size(662, 42);
            this.nomeLabel.TabIndex = 34;
            this.nomeLabel.Text = "CASSIERE: Nome Cognome 123456";
            // 
            // cassaAccesso
            // 
            this.cassaAccesso.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cassaAccesso.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cassaAccesso.FormattingEnabled = true;
            this.cassaAccesso.Location = new System.Drawing.Point(25, 201);
            this.cassaAccesso.Name = "cassaAccesso";
            this.cassaAccesso.Size = new System.Drawing.Size(136, 37);
            this.cassaAccesso.TabIndex = 36;
            this.cassaAccesso.SelectedIndexChanged += new System.EventHandler(this.Nascondi);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(20, 173);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 25);
            this.label1.TabIndex = 37;
            this.label1.Text = "Cassa";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(97, 163);
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.Size = new System.Drawing.Size(1326, 264);
            this.dataGridView1.TabIndex = 38;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 199);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(240, 25);
            this.label2.TabIndex = 39;
            this.label2.Text = "Storico Interventi Casse";
            // 
            // apri
            // 
            this.apri.Cursor = System.Windows.Forms.Cursors.Hand;
            this.apri.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.apri.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apri.Location = new System.Drawing.Point(226, 201);
            this.apri.Name = "apri";
            this.apri.Size = new System.Drawing.Size(227, 39);
            this.apri.TabIndex = 40;
            this.apri.Text = "APRI CASSA";
            this.apri.UseVisualStyleBackColor = true;
            this.apri.Click += new System.EventHandler(this.apri_Click);
            // 
            // chiudi
            // 
            this.chiudi.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chiudi.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.chiudi.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chiudi.Location = new System.Drawing.Point(538, 200);
            this.chiudi.Name = "chiudi";
            this.chiudi.Size = new System.Drawing.Size(227, 39);
            this.chiudi.TabIndex = 41;
            this.chiudi.Text = "CHIUDI CASSA";
            this.chiudi.UseVisualStyleBackColor = true;
            this.chiudi.Click += new System.EventHandler(this.chiudi_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.aggiungi);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.problemi);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.causale);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.descrizione);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.specialista);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.durata);
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.interventoCassa);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(-85, 261);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1504, 560);
            this.panel1.TabIndex = 42;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(105, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(254, 25);
            this.label3.TabIndex = 43;
            this.label3.Text = "INSERISCI INTERVENTO";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(105, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 25);
            this.label4.TabIndex = 44;
            this.label4.Text = "Cassa";
            // 
            // interventoCassa
            // 
            this.interventoCassa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.interventoCassa.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.interventoCassa.FormattingEnabled = true;
            this.interventoCassa.Location = new System.Drawing.Point(110, 84);
            this.interventoCassa.Name = "interventoCassa";
            this.interventoCassa.Size = new System.Drawing.Size(136, 33);
            this.interventoCassa.TabIndex = 43;
            // 
            // durata
            // 
            this.durata.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.durata.Location = new System.Drawing.Point(286, 84);
            this.durata.Name = "durata";
            this.durata.Size = new System.Drawing.Size(113, 31);
            this.durata.TabIndex = 45;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(286, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 25);
            this.label5.TabIndex = 46;
            this.label5.Text = "Durata";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(432, 56);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 25);
            this.label6.TabIndex = 48;
            this.label6.Text = "Specialista";
            // 
            // specialista
            // 
            this.specialista.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.specialista.Location = new System.Drawing.Point(437, 84);
            this.specialista.Name = "specialista";
            this.specialista.Size = new System.Drawing.Size(194, 31);
            this.specialista.TabIndex = 47;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(666, 56);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(125, 25);
            this.label7.TabIndex = 50;
            this.label7.Text = "Descrizione";
            // 
            // descrizione
            // 
            this.descrizione.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.descrizione.Location = new System.Drawing.Point(671, 84);
            this.descrizione.Name = "descrizione";
            this.descrizione.Size = new System.Drawing.Size(194, 31);
            this.descrizione.TabIndex = 49;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(940, 56);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 25);
            this.label8.TabIndex = 52;
            this.label8.Text = "Causale";
            // 
            // causale
            // 
            this.causale.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.causale.Location = new System.Drawing.Point(945, 84);
            this.causale.Name = "causale";
            this.causale.Size = new System.Drawing.Size(194, 31);
            this.causale.TabIndex = 51;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(1207, 56);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 25);
            this.label9.TabIndex = 54;
            this.label9.Text = "Problemi";
            // 
            // problemi
            // 
            this.problemi.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.problemi.Location = new System.Drawing.Point(1212, 84);
            this.problemi.Name = "problemi";
            this.problemi.Size = new System.Drawing.Size(194, 31);
            this.problemi.TabIndex = 53;
            // 
            // aggiungi
            // 
            this.aggiungi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.aggiungi.Cursor = System.Windows.Forms.Cursors.Hand;
            this.aggiungi.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.aggiungi.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aggiungi.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.aggiungi.Location = new System.Drawing.Point(1212, 121);
            this.aggiungi.Name = "aggiungi";
            this.aggiungi.Size = new System.Drawing.Size(194, 39);
            this.aggiungi.TabIndex = 43;
            this.aggiungi.Text = "SALVA";
            this.aggiungi.UseVisualStyleBackColor = false;
            this.aggiungi.Click += new System.EventHandler(this.aggiungi_Click);
            // 
            // errore
            // 
            this.errore.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.errore.AutoSize = true;
            this.errore.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errore.ForeColor = System.Drawing.Color.Red;
            this.errore.Location = new System.Drawing.Point(815, 197);
            this.errore.Name = "errore";
            this.errore.Size = new System.Drawing.Size(489, 42);
            this.errore.TabIndex = 43;
            this.errore.Text = "IMPOSSIBILE ACCEDERE";
            this.errore.Visible = false;
            // 
            // CassiereHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 729);
            this.Controls.Add(this.errore);
            this.Controls.Add(this.chiudi);
            this.Controls.Add(this.apri);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cassaAccesso);
            this.Controls.Add(this.nomeLabel);
            this.Controls.Add(this.dataLabel);
            this.Controls.Add(this.indietroBox);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.Name = "CassiereHome";
            this.Text = "CassiereHome";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.indietroBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox indietroBox;
        private System.Windows.Forms.Label dataLabel;
        private System.Windows.Forms.Label nomeLabel;
        private System.Windows.Forms.ComboBox cassaAccesso;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button apri;
        private System.Windows.Forms.Button chiudi;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button aggiungi;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox problemi;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox causale;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox descrizione;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox specialista;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox durata;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox interventoCassa;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label errore;
    }
}