﻿namespace MyMarket
{
    partial class Report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Report));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.provenienza = new System.Windows.Forms.Button();
            this.top = new System.Windows.Forms.Button();
            this.incassi = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(168, 126);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 24;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.Indietro);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(9, 307);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1321, 410);
            this.dataGridView1.TabIndex = 25;
            // 
            // provenienza
            // 
            this.provenienza.Cursor = System.Windows.Forms.Cursors.Hand;
            this.provenienza.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.provenienza.Location = new System.Drawing.Point(12, 157);
            this.provenienza.Name = "provenienza";
            this.provenienza.Size = new System.Drawing.Size(185, 144);
            this.provenienza.TabIndex = 26;
            this.provenienza.Text = "VISUALIZZA PROVENIENZA PRODODOTTI ";
            this.provenienza.UseVisualStyleBackColor = true;
            this.provenienza.Click += new System.EventHandler(this.provenienza_Click);
            // 
            // top
            // 
            this.top.Cursor = System.Windows.Forms.Cursors.Hand;
            this.top.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.top.Location = new System.Drawing.Point(216, 157);
            this.top.Name = "top";
            this.top.Size = new System.Drawing.Size(188, 144);
            this.top.TabIndex = 27;
            this.top.Text = "VISUALIZZA 5 PRODOTTI PIU\' VENDUTI";
            this.top.UseVisualStyleBackColor = true;
            this.top.Click += new System.EventHandler(this.top_Click);
            // 
            // incassi
            // 
            this.incassi.Cursor = System.Windows.Forms.Cursors.Hand;
            this.incassi.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.incassi.Location = new System.Drawing.Point(421, 157);
            this.incassi.Name = "incassi";
            this.incassi.Size = new System.Drawing.Size(188, 144);
            this.incassi.TabIndex = 28;
            this.incassi.Text = "VISUALIZZA TOTALE INCASSI";
            this.incassi.UseVisualStyleBackColor = true;
            this.incassi.Click += new System.EventHandler(this.incassi_Click);
            // 
            // Report
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1334, 729);
            this.Controls.Add(this.incassi);
            this.Controls.Add(this.top);
            this.Controls.Add(this.provenienza);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.pictureBox1);
            this.MaximizeBox = false;
            this.Name = "Report";
            this.Text = "Report";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button provenienza;
        private System.Windows.Forms.Button top;
        private System.Windows.Forms.Button incassi;
    }
}