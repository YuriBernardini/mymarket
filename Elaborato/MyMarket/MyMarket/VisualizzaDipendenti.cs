﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyMarket
{
    public partial class VisualizzaDipendenti : Form
    {
        MyMarketDBDataContext db = new MyMarketDBDataContext();
        public VisualizzaDipendenti()
        {
            InitializeComponent();
        }

        private void cassieri_Click(object sender, EventArgs e)
        {
            var res = from c in db.CASSIERI
                      join p in db.PERSONALE on c.Badge equals p.Badge
                      select new { Badge = c.Badge, Nome = p.Nome, Cognome = p.Cognome, DataNascita = p.DataNascita };

            dataGridView1.DataSource = res;
        }

        private void scaffalisti_Click(object sender, EventArgs e)
        {
            var res = from c in db.SCAFFALISTI
                      join p in db.PERSONALE on c.Badge equals p.Badge
                      select new { Badge = c.Badge, Nome = p.Nome, Cognome = p.Cognome, DataNascita = p.DataNascita };

            dataGridView1.DataSource = res;
        }

        private void responsabili_Click(object sender, EventArgs e)
        {
            var res = from c in db.RESPONSABILI
                      join p in db.PERSONALE on c.Badge equals p.Badge
                      select new { Badge = c.Badge, Nome = p.Nome, Cognome = p.Cognome, DataNascita = p.DataNascita };

            dataGridView1.DataSource = res;
        }

        private void Indietro(object sender, EventArgs e)
        {
            ResponsabileHome responsabile = new ResponsabileHome();
            responsabile.Show();
            this.Close();
        }
    }
}
