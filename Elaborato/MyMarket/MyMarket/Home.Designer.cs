﻿namespace MyMarket
{
    partial class Home
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Home));
            this.logo = new System.Windows.Forms.PictureBox();
            this.userTextBox = new System.Windows.Forms.TextBox();
            this.pwdTextBox = new System.Windows.Forms.TextBox();
            this.BadgeLabel = new System.Windows.Forms.Label();
            this.pwdLabel = new System.Windows.Forms.Label();
            this.enterBtn = new System.Windows.Forms.Button();
            this.erroreLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).BeginInit();
            this.SuspendLayout();
            // 
            // logo
            // 
            this.logo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.logo.Image = ((System.Drawing.Image)(resources.GetObject("logo.Image")));
            this.logo.Location = new System.Drawing.Point(483, 15);
            this.logo.Name = "logo";
            this.logo.Size = new System.Drawing.Size(400, 400);
            this.logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.logo.TabIndex = 0;
            this.logo.TabStop = false;
            // 
            // userTextBox
            // 
            this.userTextBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.userTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userTextBox.Location = new System.Drawing.Point(483, 418);
            this.userTextBox.Name = "userTextBox";
            this.userTextBox.Size = new System.Drawing.Size(400, 44);
            this.userTextBox.TabIndex = 1;
            this.userTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.userTextBox.TextChanged += new System.EventHandler(this.NascondiErrore);
            this.userTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SoloNumeri);
            // 
            // pwdTextBox
            // 
            this.pwdTextBox.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pwdTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pwdTextBox.Location = new System.Drawing.Point(483, 523);
            this.pwdTextBox.Name = "pwdTextBox";
            this.pwdTextBox.Size = new System.Drawing.Size(400, 44);
            this.pwdTextBox.TabIndex = 2;
            this.pwdTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.pwdTextBox.UseSystemPasswordChar = true;
            this.pwdTextBox.TextChanged += new System.EventHandler(this.NascondiErrore);
            // 
            // BadgeLabel
            // 
            this.BadgeLabel.AutoSize = true;
            this.BadgeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BadgeLabel.Location = new System.Drawing.Point(774, 384);
            this.BadgeLabel.Name = "BadgeLabel";
            this.BadgeLabel.Size = new System.Drawing.Size(109, 31);
            this.BadgeLabel.TabIndex = 3;
            this.BadgeLabel.Text = "BADGE";
            // 
            // pwdLabel
            // 
            this.pwdLabel.AutoSize = true;
            this.pwdLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pwdLabel.Location = new System.Drawing.Point(711, 489);
            this.pwdLabel.Name = "pwdLabel";
            this.pwdLabel.Size = new System.Drawing.Size(172, 31);
            this.pwdLabel.TabIndex = 4;
            this.pwdLabel.Text = "PASSWORD";
            // 
            // enterBtn
            // 
            this.enterBtn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.enterBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.enterBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.enterBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enterBtn.Location = new System.Drawing.Point(583, 607);
            this.enterBtn.Name = "enterBtn";
            this.enterBtn.Size = new System.Drawing.Size(200, 44);
            this.enterBtn.TabIndex = 5;
            this.enterBtn.Text = "ENTRA";
            this.enterBtn.UseVisualStyleBackColor = false;
            this.enterBtn.Click += new System.EventHandler(this.Entra);
            // 
            // erroreLabel
            // 
            this.erroreLabel.AutoSize = true;
            this.erroreLabel.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.erroreLabel.ForeColor = System.Drawing.Color.Red;
            this.erroreLabel.Location = new System.Drawing.Point(500, 654);
            this.erroreLabel.Name = "erroreLabel";
            this.erroreLabel.Size = new System.Drawing.Size(358, 42);
            this.erroreLabel.TabIndex = 7;
            this.erroreLabel.Text = "!!! DATI SBAGLIATI";
            this.erroreLabel.Visible = false;
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1350, 729);
            this.Controls.Add(this.erroreLabel);
            this.Controls.Add(this.enterBtn);
            this.Controls.Add(this.pwdLabel);
            this.Controls.Add(this.BadgeLabel);
            this.Controls.Add(this.pwdTextBox);
            this.Controls.Add(this.userTextBox);
            this.Controls.Add(this.logo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Home";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MyMarket";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.logo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox logo;
        private System.Windows.Forms.TextBox userTextBox;
        private System.Windows.Forms.TextBox pwdTextBox;
        private System.Windows.Forms.Label BadgeLabel;
        private System.Windows.Forms.Label pwdLabel;
        private System.Windows.Forms.Button enterBtn;
        private System.Windows.Forms.Label erroreLabel;
    }
}

