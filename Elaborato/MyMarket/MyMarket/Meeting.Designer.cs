﻿namespace MyMarket
{
    partial class Meeting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Meeting));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.aggiorna = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.titolo = new System.Windows.Forms.TextBox();
            this.propostaCliente = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.propostaRappresentante = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.motivo = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.conclusione = new System.Windows.Forms.TextBox();
            this.idResponsabile = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.idRappresentante = new System.Windows.Forms.ComboBox();
            this.errore = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.rappresentante = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.responsabile = new System.Windows.Forms.TextBox();
            this.indietroBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.indietroBox)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 379);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1326, 338);
            this.dataGridView1.TabIndex = 0;
            // 
            // aggiorna
            // 
            this.aggiorna.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aggiorna.Location = new System.Drawing.Point(1069, 159);
            this.aggiorna.Name = "aggiorna";
            this.aggiorna.Size = new System.Drawing.Size(269, 109);
            this.aggiorna.TabIndex = 2;
            this.aggiorna.Text = "AGGIUNGI";
            this.aggiorna.UseVisualStyleBackColor = true;
            this.aggiorna.Click += new System.EventHandler(this.aggiorna_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 345);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(257, 31);
            this.label1.TabIndex = 3;
            this.label1.Text = "ELENCO MEETING";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(596, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(283, 31);
            this.label2.TabIndex = 4;
            this.label2.Text = "INSERISCI MEETING";
            // 
            // titolo
            // 
            this.titolo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titolo.Location = new System.Drawing.Point(8, 159);
            this.titolo.Name = "titolo";
            this.titolo.Size = new System.Drawing.Size(200, 31);
            this.titolo.TabIndex = 5;
            // 
            // propostaCliente
            // 
            this.propostaCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.propostaCliente.Location = new System.Drawing.Point(8, 229);
            this.propostaCliente.Name = "propostaCliente";
            this.propostaCliente.Size = new System.Drawing.Size(200, 31);
            this.propostaCliente.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(22, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 24);
            this.label3.TabIndex = 7;
            this.label3.Text = "Titolo";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 202);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(204, 24);
            this.label4.TabIndex = 8;
            this.label4.Text = "Proposta Responsabile";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 279);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(222, 24);
            this.label5.TabIndex = 10;
            this.label5.Text = "Proposta Rappresentante";
            // 
            // propostaRappresentante
            // 
            this.propostaRappresentante.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.propostaRappresentante.Location = new System.Drawing.Point(12, 306);
            this.propostaRappresentante.Name = "propostaRappresentante";
            this.propostaRappresentante.Size = new System.Drawing.Size(200, 31);
            this.propostaRappresentante.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(323, 132);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 24);
            this.label6.TabIndex = 12;
            this.label6.Text = "Motivo";
            // 
            // motivo
            // 
            this.motivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.motivo.Location = new System.Drawing.Point(309, 159);
            this.motivo.Name = "motivo";
            this.motivo.Size = new System.Drawing.Size(200, 31);
            this.motivo.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(602, 132);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(116, 24);
            this.label7.TabIndex = 14;
            this.label7.Text = "Conclusione";
            // 
            // conclusione
            // 
            this.conclusione.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.conclusione.Location = new System.Drawing.Point(602, 159);
            this.conclusione.Name = "conclusione";
            this.conclusione.Size = new System.Drawing.Size(200, 31);
            this.conclusione.TabIndex = 13;
            // 
            // idResponsabile
            // 
            this.idResponsabile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.idResponsabile.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idResponsabile.FormattingEnabled = true;
            this.idResponsabile.Location = new System.Drawing.Point(360, 230);
            this.idResponsabile.Name = "idResponsabile";
            this.idResponsabile.Size = new System.Drawing.Size(112, 33);
            this.idResponsabile.TabIndex = 15;
            this.idResponsabile.SelectedIndexChanged += new System.EventHandler(this.aggiornaResponsabile);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(323, 202);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(145, 24);
            this.label8.TabIndex = 16;
            this.label8.Text = "Id Responsabile";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(323, 279);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(163, 24);
            this.label9.TabIndex = 18;
            this.label9.Text = "Id Rappresentante";
            // 
            // idRappresentante
            // 
            this.idRappresentante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.idRappresentante.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idRappresentante.FormattingEnabled = true;
            this.idRappresentante.Location = new System.Drawing.Point(360, 306);
            this.idRappresentante.Name = "idRappresentante";
            this.idRappresentante.Size = new System.Drawing.Size(112, 33);
            this.idRappresentante.TabIndex = 17;
            this.idRappresentante.SelectedIndexChanged += new System.EventHandler(this.aggiornaRappresentante);
            // 
            // errore
            // 
            this.errore.AutoSize = true;
            this.errore.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errore.ForeColor = System.Drawing.Color.Red;
            this.errore.Location = new System.Drawing.Point(1019, 84);
            this.errore.Name = "errore";
            this.errore.Size = new System.Drawing.Size(328, 31);
            this.errore.TabIndex = 19;
            this.errore.Text = "ERRORE INSERIMENTO";
            this.errore.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(602, 279);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(143, 24);
            this.label10.TabIndex = 23;
            this.label10.Text = "Rappresentante";
            // 
            // rappresentante
            // 
            this.rappresentante.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rappresentante.Location = new System.Drawing.Point(602, 306);
            this.rappresentante.Name = "rappresentante";
            this.rappresentante.ReadOnly = true;
            this.rappresentante.Size = new System.Drawing.Size(330, 31);
            this.rappresentante.TabIndex = 22;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(602, 203);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(125, 24);
            this.label11.TabIndex = 21;
            this.label11.Text = "Responsabile";
            // 
            // responsabile
            // 
            this.responsabile.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.responsabile.Location = new System.Drawing.Point(602, 228);
            this.responsabile.Name = "responsabile";
            this.responsabile.ReadOnly = true;
            this.responsabile.Size = new System.Drawing.Size(330, 31);
            this.responsabile.TabIndex = 20;
            // 
            // indietroBox
            // 
            this.indietroBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.indietroBox.Image = ((System.Drawing.Image)(resources.GetObject("indietroBox.Image")));
            this.indietroBox.Location = new System.Drawing.Point(12, 12);
            this.indietroBox.Name = "indietroBox";
            this.indietroBox.Size = new System.Drawing.Size(168, 126);
            this.indietroBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.indietroBox.TabIndex = 24;
            this.indietroBox.TabStop = false;
            this.indietroBox.Click += new System.EventHandler(this.indietroBox_Click);
            // 
            // Meeting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 729);
            this.Controls.Add(this.indietroBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.rappresentante);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.responsabile);
            this.Controls.Add(this.errore);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.idRappresentante);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.idResponsabile);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.conclusione);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.motivo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.propostaRappresentante);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.propostaCliente);
            this.Controls.Add(this.titolo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.aggiorna);
            this.Controls.Add(this.dataGridView1);
            this.MaximizeBox = false;
            this.Name = "Meeting";
            this.Text = "Meeting";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.indietroBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button aggiorna;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox titolo;
        private System.Windows.Forms.TextBox propostaCliente;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox propostaRappresentante;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox motivo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox conclusione;
        private System.Windows.Forms.ComboBox idResponsabile;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox idRappresentante;
        private System.Windows.Forms.Label errore;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox rappresentante;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox responsabile;
        private System.Windows.Forms.PictureBox indietroBox;
    }
}