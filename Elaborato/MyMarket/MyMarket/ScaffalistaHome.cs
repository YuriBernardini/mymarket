﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyMarket
{
    public partial class ScaffalistaHome : Form
    {
        public ScaffalistaHome()
        {
            InitializeComponent();
            this.nameLabel.Text = AppData.Instance.NomeUtenteAttivo + " " + AppData.Instance.CognomeUtenteAttivo;
            this.dateLabel.Text = AppData.Instance.DataSelezionata.ToString("dd'/'MM'/'yyyy");
        }

        private void Indietro(object sender, EventArgs e)
        {
            Home main = new Home();
            main.Show();
            this.Close();
        }

        private void InserisciNuovoProdotto(object sender, EventArgs e)
        {
            InserimentoNuovoProdotto nuovoProd = new InserimentoNuovoProdotto();
            nuovoProd.Show();
            this.Close();
        }

        private void ApriGestioneProdotto(object sender, EventArgs e)
        {
            GestioneProdotto gestProd = new GestioneProdotto();
            gestProd.Show();
            this.Close();
        }
        private void ApriGestioneCorsie(object sender, EventArgs e)
        {
            GestioneCorsie cors = new GestioneCorsie();
            cors.Show();
            this.Close();
        }

    }
}
